#include "cvlutils.h"

#include <QDebug>
#include <queue>
#include <eigen3/Eigen/Dense>


#include "qutils.h"

namespace cvl {

    ///*************************************************************************
    ///*****                    CONST DATA                **********************
    ///*************************************************************************

    const double sobelGradXData[] = {-1, 0, 1,
                                     -2, 0, 2,
                                     -1, 0, 1};
    const Matrix sobelGradXMatrix(3, 3, sobelGradXData);

    const double sobelGradYData[] = {-1, -2, -1,
                                      0,  0,  0,
                                      1,  2,  1};
    const Matrix sobelGradYMatrix(3, 3, sobelGradYData);

    const int di8[] = {-1, -1, -1,  0, 0,  1, 1, 1};
    const int dj8[] = {-1,  0,  1, -1, 1, -1, 0, 1};

    const int di26[] = {-1, -1, -1,  0,  0,  0,  1,  1,  1,
                        -1, -1, -1,  0,  0,  1,  1,  1,
                        -1, -1, -1,  0,  0,  0,  1,  1,  1};
    const int dj26[] = {-1,  0,  1, -1,  0,  1, -1,  0,  1,
                        -1,  0,  1, -1,  1, -1,  0,  1,
                        -1,  0,  1, -1,  0,  1, -1,  0,  1};
    const int dk26[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1,
                         0,  0,  0,  0,  0,  0,  0,  0,
                         1,  1,  1,  1,  1,  1,  1,  1,  1};

    ///*************************************************************************
    ///*****                COMMON FUNCTIONS              **********************
    ///*************************************************************************

    inline double sqr(double x) {
        return x * x;
    }

    inline bool inMatrix(const Matrix &matrix, const int &i, const int &j)
    {
        return i >= 0 && i < matrix.getRowSize() && j >=0 && j < matrix.getColumnSize();
    }

    inline double getValue(const Matrix &matrix, int i, int j, TypeBorder typeBorder = TypeBorder::ZERO_BORDER)
    {
        if (inMatrix(matrix, i, j))
            return matrix.get(i, j);

        switch (typeBorder) {
        case TypeBorder::ZERO_BORDER:
            return 0;
        case TypeBorder::COPY_BORDER:
            i = std::max(0, std::min(matrix.getRowSize() - 1, i));
            j = std::max(0, std::min(matrix.getColumnSize() - 1, j));
            return matrix.get(i, j);
        case TypeBorder::REFLECT_BORDER:
            if (i < 0) i *= -1;
            if (j < 0) j *= -1;
            if (i >= matrix.getRowSize()) i = 2 * matrix.getRowSize() - i - 1;
            if (j >= matrix.getColumnSize()) j = 2 * matrix.getColumnSize() - j - 1;
            return matrix.get(i, j);
        case TypeBorder::WRAP_BORDER:
            if (i < 0) i = matrix.getRowSize() + i;
            if (j < 0) j = matrix.getColumnSize() + j;
            if (i >= matrix.getRowSize()) i = i - matrix.getRowSize();
            if (j >= matrix.getColumnSize()) j = j - matrix.getColumnSize();
            return matrix.get(i, j);
        }
    }

    void findLocalMaximum(vector<Point<int> > &findPoints, vector<double> &weights, const Matrix &matrix, int r, double t, int border)
    {
        findPoints.clear();
        weights.clear();
        const int rowSize = matrix.getRowSize();
        const int columnSize = matrix.getColumnSize();
        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j) {
                const double value = matrix.get(i, j);
                if (i < border || (rowSize - i - 1) < border || j < border || (columnSize - j - 1) < border)
                    goto notMaximum;
                if (value < t)
                    goto notMaximum;
                for (int di = -r; di <= r; ++di)
                    for (int dj = -r; dj <= r; ++dj) {
                        const int toI = i + di,
                                  toJ = j + dj;
                        if (inMatrix(matrix, toI, toJ) && value < matrix.get(toI, toJ))
                            goto notMaximum;
                    }
                findPoints.push_back(Point<int>(i, j));
                weights.push_back(value);
                notMaximum:;
            }
    }

    ///*************************************************************************
    ///*****                   CONVOLUTIONS               **********************
    ///*************************************************************************

    inline double calcElement(const Matrix &src, int di, int dj, int i, int j, const Matrix &kernel, int rowSize, int columnSize, const TypeBorder typeBorder)
    {
        double value = 0;
        for (int u = 0 ; u < rowSize; ++u)
            for (int v = 0; v < columnSize; ++v)
                value += getValue(src, i + di - u, j + dj - v, typeBorder) * kernel.get(u, v);
        return value;
    }

    void convolution(Matrix &dest, const Matrix &src, const Matrix &kernel, const TypeBorder typeBorder)
    {
        const int rowSize = src.getRowSize();
        const int columnSize = src.getColumnSize();
        dest.resize(rowSize, columnSize);

        const int kernelRowSize = kernel.getRowSize();
        const int kernelColumnSize = kernel.getColumnSize();
        const int di = kernelRowSize / 2;
        const int dj = kernelColumnSize / 2;

        double *destBuffer = dest.getBuffer();
        const double *kernelBuffer = kernel.getBuffer();
        const double *srcBuffer = src.getBuffer();

        // In a rectangle [i0, j0] - (i1, j1) kernel matrix
        // place inside a src matrix.
        //       +---------+
        //       |         |
        //       |  +---+  |
        // i0 -> |  |   |  |
        //       |  +---+  |
        // i1 -> |         |
        //       +---------+
        //           ^   ^
        //           j0  j1
        int i0 = kernelRowSize - 1 - di,
            i1 = rowSize - di,
            j0 = kernelColumnSize - 1 - dj,
            j1 = columnSize - dj;

        // top rectangle
        // +---------+
        // |xxxxxx|  |
        // |--+---+  |
        // |  |   |  |
        // |  +---+  |
        // |         |
        // +---------+
        for (int i = 0; i < i0; ++i)
            for (int j = 0; j < j1; ++j) {
                double value = calcElement(src, di, dj, i, j, kernel, kernelRowSize, kernelColumnSize, typeBorder);
                dest.set(i, j, value);
            }

        // right rectangle
        // +---------+
        // |      |xx|
        // |  +---+xx|
        // |  |   |xx|
        // |  +---+--|
        // |         |
        // +---------+
        for (int j = j1; j < columnSize; ++j)
            for (int i = 0; i < i1; ++i) {
                double value = calcElement(src, di, dj, i, j, kernel, kernelRowSize, kernelColumnSize, typeBorder);
                dest.set(i, j, value);
            }

        // bottom rectangle
        // +---------+
        // |         |
        // |  +---+  |
        // |  |   |  |
        // |  +---+--|
        // |  |xxxxxx|
        // +---------+
        for (int i = i1; i < rowSize; ++i)
            for (int j = j0; j < columnSize; ++j) {
                double value = calcElement(src, di, dj, i, j, kernel, kernelRowSize, kernelColumnSize, typeBorder);
                dest.set(i, j, value);
            }

        // left rectangle
        // +---------+
        // |         |
        // |--+---+  |
        // |xx|   |  |
        // |xx+---+  |
        // |xx|      |
        // +---------+
        for (int j = 0; j < j0; ++j)
            for (int i = i0; i < rowSize; ++i) {
                double value = calcElement(src, di, dj, i, j, kernel, kernelRowSize, kernelColumnSize, typeBorder);
                dest.set(i, j, value);
            }

        // center rectangle
        // +---------+
        // |         |
        // |  +---+  |
        // |  |xxx|  |
        // |  +---+  |
        // |         |
        // +---------+
        for (int i = i0; i < i1; ++i)
            for (int j = j0, dIndex = i * columnSize + j; j < j1; ++j, ++dIndex) {
                double value = 0;

                int kIndex = 0;
                for (int u = 0 ; u < kernelRowSize; ++u)
                    for (int v = 0, sIndex = (i + di - u) * columnSize + j + dj - v; v < kernelColumnSize; ++v, --sIndex)
                        value += srcBuffer[sIndex] * kernelBuffer[kIndex++];

                destBuffer[dIndex] = value;
            }
    }

    void convolutionSlow(Matrix &dest, const Matrix &src, const Matrix &kernel, const TypeBorder typeBorder)
    {
        const int rowSize = src.getRowSize();
        const int columnSize = src.getColumnSize();
        dest.resize(rowSize, columnSize);

        const int kernelRowSize = kernel.getRowSize();
        const int kernelColumnSize = kernel.getColumnSize();
        const int di = kernelRowSize / 2;
        const int dj = kernelColumnSize / 2;

        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j) {
                double value = 0;

                for (int u = 0 ; u < kernelRowSize; ++u)
                    for (int v = 0; v < kernelColumnSize; ++v)
                        value += getValue(src, i + di - u, j + dj - v, typeBorder) * kernel.get(u, v);

                dest.set(i, j, value);
            }
    }

    ///*************************************************************************
    ///*****                  GAUSSIAN BLUR               **********************
    ///*************************************************************************

    void gaussKernel(Matrix &kernel, int r)
    {
        int size = r * 2 + 1;
        kernel.resize(size, size);
        double sigma = size / 3.0;
        const double b = 2 * M_PI * sigma * sigma;
        double sum = 0;
        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j) {
                double x = size / 2 - i;
                double y = size / 2 - j;
                double a = exp(-(x * x + y * y) / (2 * sigma * sigma));
                kernel.set(i, j, a / b);
                sum += a / b;
            }
        kernel.scalarDivide(sum);
    }

    void gaussBlur(Matrix &dest, const Matrix &src, double sigma, const TypeBorder typeBorder)
    {
        Matrix buffer;
        gaussBlur(dest, src, sigma, buffer, typeBorder);
    }

    void gaussBlur(Matrix &dest, const Matrix &src, int r, const TypeBorder typeBorder)
    {
        Matrix buffer;
        gaussBlur(dest, src, r, buffer, typeBorder);
    }

    void gaussBlur(Matrix &dest, const Matrix &src, int r, double sigma, const TypeBorder typeBorder)
    {
        Matrix buffer;
        gaussBlur(dest, src, r, sigma, buffer, typeBorder);
    }

    void gaussBlur(Matrix &dest, const Matrix &src, double sigma, Matrix &buffer, const TypeBorder typeBorder)
    {
        int r = (int)((sigma + 0.5) * 3);
        int maxR = std::min(src.getRowSize(), src.getColumnSize()) / 2 - 1;
        r = std::max(std::min(r, maxR), 1);
        gaussBlur(dest, src, r, sigma, buffer, typeBorder);
    }

    void gaussBlur(Matrix &dest, const Matrix &src, int r, Matrix &buffer, const TypeBorder typeBorder)
    {
        double sigma = r / 3.0;
        gaussBlur(dest, src, r, sigma, buffer, typeBorder);
    }

    void gaussBlur(Matrix &dest, const Matrix &src, int r, double sigma, Matrix &buffer, const TypeBorder typeBorder)
    {
        int size = r * 2 - 1;
        auto data = std::make_unique<double[]>(size);
        const double b = sqrt(2 * M_PI) * sigma;
        double sum = 0;
        for (int i = 0; i < size ; ++i) {
            double x = size / 2 - i;
            double a = exp(-x * x / (2 * sigma * sigma));
            data[i] = a / b;
            sum += data[i];
        }
        for (int i = 0; i < size ; ++i) {
            data[i] /= sum;
        }

        Matrix row(1, size, data.get());
        Matrix column(size, 1, std::move(data));

        convolution(buffer, src, row, typeBorder);
        convolution(dest, buffer, column, typeBorder);
    }

    void gaussBlurSlow(Matrix &dest, const Matrix &src, double sigma, const TypeBorder typeBorder)
    {
        int size = std::max((int)((sigma + 0.5)) * 6 - 1, 5);
        int maxSize = std::min(src.getRowSize(), src.getColumnSize());
        if (maxSize % 2 == 0)
            maxSize--;
        size = std::min(size, maxSize);

        Matrix kernel(size, size);
        const double b = 2 * M_PI * sigma * sigma;
        double sum = 0;
        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j) {
                double x = size / 2 - i;
                double y = size / 2 - j;
                double a = exp(-(x * x + y * y) / (2 * sigma * sigma));
                kernel.set(i, j, a / b);
                sum += a / b;
            }
        kernel.scalarDivide(sum);

        convolutionSlow(dest, src, kernel, typeBorder);
    }

    ///*************************************************************************
    ///*****                  RESIZE MATRIX               **********************
    ///*************************************************************************

    void downSample(Matrix &dest, const Matrix &src)
    {
        const int rowSize = src.getRowSize();
        const int columnSize = src.getColumnSize();
        dest.resize((rowSize + 1) / 2, (columnSize + 1) / 2);
        for (int di = 0, si = 0; si < rowSize; ++di, si += 2)
            for (int dj = 0, sj = 0; sj < columnSize; ++dj, sj += 2)
                dest.set(di, dj, src.get(si, sj));
    }

    ///*************************************************************************
    ///*****                  SOBEL OPERATORS             **********************
    ///*************************************************************************

    void sobelGradX(Matrix &dest, const Matrix &src, const TypeBorder typeBorder)
    {
        convolution(dest, src, sobelGradXMatrix, typeBorder);
    }

    void sobelGradY(Matrix &dest, const Matrix &src, const TypeBorder typeBorder)
    {
        convolution(dest, src, sobelGradYMatrix, typeBorder);
    }

    void sobel(Matrix &dest, const Matrix &src, const TypeBorder typeBorder)
    {
        Matrix gradX, gradY;
        sobel(dest, src, gradX, gradY, typeBorder);
    }

    void sobel(Matrix &dest, const Matrix &src, Matrix &gradX, Matrix &gradY, const TypeBorder typeBorder)
    {
        sobelGradX(gradX, src, typeBorder);
        sobelGradY(gradY, src, typeBorder);
        dest.hypot(gradX, gradY);
    }

    ///*************************************************************************
    ///*****                CANNY EDGE DETECTER           **********************
    ///*************************************************************************

    void cannyEdgeDetecter(Matrix &dest, const Matrix &src, double low, double high, const TypeBorder typeBorder)
    {
        Matrix gradX, gradY;
        cannyEdgeDetecter(dest, src, gradX, gradY, low, high, typeBorder);
    }

    inline int getDi(double a) {
        const double minAngle = M_PI / 8;
        const double maxAngle = 7 * M_PI / 8;
        if (minAngle <= a && a <= maxAngle)
            return 1;
        if (-maxAngle <= a && a <= -minAngle)
            return -1;
        return 0;
    }

    inline int getDj(double a) {
        const double minAngle = 3 * M_PI / 8;
        const double maxAngle = 5 * M_PI / 8;
        if (-minAngle <= a && a <= minAngle)
            return 1;
        if (maxAngle <= a || a <= -maxAngle)
            return -1;
        return 0;
    }

    void cannyEdgeDetecter(Matrix &dest, const Matrix &src, Matrix &gradX, Matrix &gradY, double low, double high, const TypeBorder typeBorder)
    {
        Matrix buffer;
        sobel(buffer, src, gradX, gradY, typeBorder);
//        buffer.normalize(0, 1);

        const int rowSize = src.getRowSize();
        const int columnSize = src.getColumnSize();
        dest.resize(rowSize, columnSize);
        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j) {
                double value = buffer.get(i, j);
                if (value > low) {

                    double a = atan2(gradY.get(i, j), gradX.get(i, j));
                    const int di = getDi(a);
                    const int dj = getDj(a);

                    if (getValue(buffer, i - di, j - dj, typeBorder) >= value || getValue(buffer, i + di, j + dj, typeBorder) >= value)
                        value = 0;
                    else
                        value = value > high ? 1 : 0.5;

                } else
                    value = 0;

                dest.set(i, j, value);
            }

        // bfs???
        struct Point {int i, j; Point(int i, int j) : i(i), j(j){}};
        const int di[] = {-1, -1, -1,  0, 0,  1, 1, 1};
        const int dj[] = {-1,  0,  1, -1, 1, -1, 0, 1};
        const int cnt = 8;
        std::queue<Point> q;
        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j)
                if (dest.get(i, j) == 1)
                    q.push(Point(i, j));
        while (!q.empty()) {
            Point p = q.back();
            q.pop();
            for (int k = 0; k < cnt; ++k) {
                int toI = p.i + di[k];
                int toJ = p.j + dj[k];
                if (inMatrix(dest, toI, toJ))
                    if (dest.get(toI, toJ) == 0.5) {
                        dest.set(toI, toJ, 1);
                        q.push(Point(toI, toJ));
                    }
            }
        }

        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j)
                if (dest.get(i, j) == 0.5)
                    dest.set(i, j, 0);

    }

    ///*************************************************************************
    ///*****                MORAVEK OPERATORS             **********************
    ///*************************************************************************

    void moravekOperator(Matrix &dest, const Matrix &src, int r, const TypeBorder typeBorder)
    {
        const int rowSize = src.getRowSize();
        const int columnSize = src.getColumnSize();
        dest.resize(rowSize, columnSize);
        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j) {
                double minValue = INFINITY;
                for (int k = 0; k < 8; ++k) {
                    double value = 0;
                    for (int u = -r; u <= r; ++u)
                        for (int v = -r; v <= r; ++v)
                            value += sqr(getValue(src, i + u, j + v, typeBorder) - getValue(src, i + u + di8[k], j + v + dj8[k], typeBorder));
                    minValue = std::min(minValue, value);
                }
                dest.set(i, j, minValue);
            }

//        findLocalMaximum(findPoints, weights, buffer, t);
    }

    ///*************************************************************************
    ///*****                HARRIS OPERATORS              **********************
    ///*************************************************************************

    void harrisOperator(Matrix &dest, const Matrix &src, int r, const TypeHarrisOperator typeOperator, const TypeBorder typeBorder)
    {
        Matrix blur;
        gaussBlur(blur, src, 3);

        Matrix gradX, gradY;
        sobelGradX(gradX, blur, typeBorder);
        sobelGradY(gradY, blur, typeBorder);

        const int rowSize = src.getRowSize();
        const int columnSize = src.getColumnSize();
        Matrix matrixA(rowSize, columnSize),    // matrix with 'a' coefficients
               matrixB(rowSize, columnSize),    // ...
               matrixC(rowSize, columnSize),    // ...
               buffer(rowSize, columnSize);     // temp buffer

        buffer.sqrSumm(gradX, gradX); // buffer is matrix of (Ix)^2
        gaussBlur(matrixA, buffer, r, typeBorder); // matrixA have weighted sum (Ix)^2
        buffer.scalarMultiply(gradX, gradY);
        gaussBlur(matrixB, buffer, r, typeBorder);
        buffer.sqrSumm(gradY, gradY);
        gaussBlur(matrixC, buffer, r, typeBorder);

//        int size = r * 2 + 1;
//        Matrix kernel(size, size);
//        double sigma = 1;
//        const double b = 2 * M_PI * sigma * sigma;
//        double sum = 0;
//        for (int i = 0; i < size; ++i)
//            for (int j = 0; j < size; ++j) {
//                double x = size / 2 - i;
//                double y = size / 2 - j;
//                double a = exp(-(x * x + y * y) / (2 * sigma * sigma));
//                kernel.set(i, j, a / b);
//                sum += a / b;
//            }
//        kernel.scalarDivide(sum);

//        for (int i = 0; i < rowSize; ++i)
//            for (int j = 0; j < columnSize; ++j) {
//                double a = 0, b = 0, c = 0;
//                for (int di = -r; di <= r; ++di)
//                    for (int dj = -r; dj <= r; ++dj) {
//                        a += getValue(gradX, i + di, j + dj) * getValue(gradX, i + di, j + dj) * kernel.get(r + di, r + dj);
//                        b += getValue(gradX, i + di, j + dj) * getValue(gradY, i + di, j + dj) * kernel.get(r + di, r + dj);
//                        c += getValue(gradY, i + di, j + dj) * getValue(gradY, i + di, j + dj) * kernel.get(r + di, r + dj);
//                    }
//                matrixA.set(i, j, a);
//                matrixB.set(i, j, b);
//                matrixC.set(i, j, c);
//            }

        // find a lambda values
        dest.resize(rowSize, columnSize);
        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j) {

                // a b
                // b c
                const double a = matrixA.get(i, j);
                const double b = matrixB.get(i, j);
                const double c = matrixC.get(i, j);

                double value = 0;
                switch (typeOperator) {
                case TypeHarrisOperator::ORIGINAL:
                    value = a * c - b * b - 0.06 * (a + b) * (a + b);
                    break;
                case TypeHarrisOperator::LAMBDA_MIN:
                    // for find eigenvalues solve the quadratic equation:
                    // |a-l  b |
                    // | b  c-l| = 0
                    // or l^2 - l*(a + c) + (ac - b^2) = 0
                    const double d = sqrt(sqr(a - c) + 4 * b * b); // d >= 0
                    const double lambda0 = (a + c - d) / 2;
                    const double lambda1 = (a + c + d) / 2;
                    value = min(fabs(lambda0), fabs(lambda1));
                    break;
                }
                dest.set(i, j, value);
            }
    }

    pair<vector<Point<int> >, vector<double> > nonMaximumSuppression(const vector<Point<int> > &points, const vector<double> &weights, int count, double r0, double scale, double d)
    {
        vector<Point<int>> findPoints = points;
        vector<double> findWeights = weights;

        double maxR = 0;
        for (int i = 0; i < findPoints.size(); ++i)
            for (int j = 0; j < findPoints.size(); ++j)
                maxR = std::max(maxR, sqr(findPoints[i].i - findPoints[j].i) + sqr(findPoints[i].j - findPoints[j].j));

        double r = r0 * r0;
        while (findPoints.size() > count && r < maxR) {
            for (int i = 0; i < findPoints.size() && findPoints.size() > count; ++i) {
                for (int j = i; j < findPoints.size(); ++j) {
                    if (i != j)
                        if (sqr(findPoints[i].i - findPoints[j].i) + sqr(findPoints[i].j - findPoints[j].j) <= r)
                            if (findWeights[i] * d < findWeights[j]) {
                                findPoints.erase(findPoints.begin() + i);
                                findWeights.erase(findWeights.begin() + i);
                                --i;
                                goto notMaximum;
                            }
                    notMaximum:;
                }
            }
            r = r * scale * scale;
        }


        return make_pair(findPoints, findWeights);
    }

    ///*************************************************************************
    ///*****                   DESCRIPTORS                **********************
    ///*************************************************************************

    inline double getParabolaMax(double y0, double y1, double y2) {
        auto a = (y0 - 2 * y1 + y2) / 2,
             b = (y2 - y0) / 2;
        return -b / (2 * a);
    }

    inline double alpha02pi(double alpha) {
        return alpha < 0 ? (alpha + 2 * M_PI) : (alpha >= 2 * M_PI ? (alpha - 2 * M_PI) : alpha);
    }

    inline double getAngle(double bin[], int numOrientation, int orientation) {
        double y0 = bin[(numOrientation + orientation - 1) % numOrientation],
               y1 = bin[orientation],
               y2 = bin[(orientation + 1) % numOrientation],
               alpha = (getParabolaMax(y0, y1, y2) + orientation) * 2 * M_PI / numOrientation;
        return alpha02pi(alpha);
    }

    void getDescriptors(vector<vector<double>> &descriptors, const Matrix &src, vector<Point<int>> &points, int r, int o, const TypeBorder typeBorder)
    {
        Matrix kernel;
        Matrix gradX, gradY;
        gaussKernel(kernel, r);

        Matrix blur;
        gaussBlur(blur, src, 2);

        sobelGradX(gradX, blur, typeBorder);
        sobelGradY(gradY, blur, typeBorder);

        // mb assign
        descriptors.clear();
        for (Point<int> point : points) {
            int posI = point.i;
            int posJ = point.j;
            vector<double> descriptor;
            descriptor.resize(4 * o, 0);
            for (int i = posI - r, ki = 0; i <= posI + r; ++i, ++ki)
                for (int j = posJ - r, kj = 0; j <= posJ + r; ++j, ++kj) {

                    int gistogram;
                    if (i < posI)
                        gistogram = j < posJ ? 2 : 1;
                    else
                        gistogram = j < posJ ? 3 : 0;

                    double dx = getValue(gradX, i, j, typeBorder),
                           dy = getValue(gradY, i, j, typeBorder);
                    double angle = (atan2(dy, dx) + 2 * M_PI) / o;

                    double len = sqrt(dx * dx + dy * dy) * kernel.get(ki, kj);
                    int bin = (int) angle;
                    double val1 = (angle - bin) * len,
                           val2 = len - val1;

                    descriptor[gistogram * o + bin % o] += val1;
                    descriptor[gistogram * o + (bin + 1) % o] += val2;

                }

            auto maxValue = *max_element(begin(descriptor), end(descriptor));
            int cntHigh = 0;
            for_each(begin(descriptor), end(descriptor), [&](double &x) {x /= maxValue; if (x > 0.5) cntHigh++;});
            if (cntHigh <= 2)
                for_each(begin(descriptor), end(descriptor), [&](double &x) {x = min(x, 0.5) * 2;});

            descriptors.push_back(descriptor);
        }
    }

    void getAngles(vector<double> &angles, const Matrix &gradX, const Matrix &gradY, int posI, int posJ, int r, int o, const TypeBorder typeBorder)
    {
        const int numOrientation = o;
        double bin[numOrientation];
        memset(bin, 0, numOrientation * sizeof(double));
        for (int i = posI - r; i <= posI + r; ++i)
            for (int j = posJ - r; j <= posJ + r; ++j) {
                auto dx = getValue(gradX, i, j, typeBorder),
                     dy = getValue(gradY, i, j, typeBorder);
                auto len = sqrt(dx * dx + dy * dy);
                auto a = atan2(dy, dx);
                while (a < 0) a += 2 * M_PI;
                a = a * numOrientation / (2 * M_PI);
                int inta = ((int) a);
                auto vall = (a - inta) * len,
                     valr = len - vall;
                bin[inta] += vall;
                bin[(inta + 1) % numOrientation] += valr;
            }

        // main orientation
        int mainOrientation = (max_element(bin, bin + numOrientation) - bin);
        angles.push_back(getAngle(bin, numOrientation, mainOrientation));

        // second orientation >= 0.8
        int secondOrientation = -1;
        for (int i = 0; i < numOrientation; ++i)
            if (i != mainOrientation && (secondOrientation == -1 || bin[secondOrientation] < bin[i])) // maximum
                if (bin[(numOrientation + secondOrientation - 1) % numOrientation] < bin[secondOrientation] // local maximum
                        && bin[secondOrientation] > bin[(secondOrientation + 1) & numOrientation])
                    secondOrientation = i;
        if (bin[secondOrientation] < bin[mainOrientation] * 0.8)
            return;
        angles.push_back(getAngle(bin, numOrientation, secondOrientation));

        qDebug() << mainOrientation << secondOrientation;
    }



    void getDescriptors(vector<Descriptor> &descriptors, const Matrix &gradX, const Matrix &gradY, vector<Point<double> > &points, double scale, int r, const TypeBorder typeBorder)
    {
        vector<double> angles;
        for (auto point : points) {
            int posI = point.i + 0.5;
            int posJ = point.j + 0.5;
            angles.clear();
            getAngles(angles, gradX, gradY, posI, posJ, r, 48, typeBorder);
            for (auto angle : angles) {
                descriptors.emplace_back(posI, posJ, angle);
                Descriptor &descriptor = descriptors.back();
                descriptor.scale = scale;
                auto cosa = cos(angle),
                     sina = sin(angle);
                int R = r * 3 / 2;
                for (int i = posI - R, di = -R; i <= posI + R; ++i, ++di)
                    for (int j = posJ - R, dj = -R; j <= posJ + R; ++j, ++dj) {
                        auto x = ((double) dj) / r;
                        auto y = ((double) di) / r;
                        auto dx = getValue(gradX, i, j, typeBorder),
                             dy = getValue(gradY, i, j, typeBorder);
                        auto a = atan2(dy, dx) - angle;
                        while (a < 0) a += 2 * M_PI;
                        auto len = sqrt(dx * dx + dy * dy);
                        descriptor.add(x * cosa + y * sina, -x * sina + y * cosa, a, len);
                    }
                descriptor.normalize();
            }
        }

    }


    void getDescriptors(vector<Descriptor> &descriptors, const Matrix &src, int scalesPerOctave, int r, double s0, double s, double t, const TypeBorder typeBorder)
    {
        int size = min(src.getRowSize(), src.getColumnSize());
        int countOctaves = log2(size) - 5;

        int border = r;

        ScalePyramid sp;
        sp.build(src, countOctaves, scalesPerOctave, s0, s);
        auto dog = std::make_unique<Matrix[]>(scalesPerOctave + 2);

        vector<Point<double>> points;
        Matrix gradX, gradY, grdXX, gradXY, gradYY;
        for (int octave = 0; octave < countOctaves; ++octave) {



            Matrix *scales = &sp.getScale(octave, 0); // pointer to octave
            for (int scale = 0; scale < scalesPerOctave + 2; ++scale) {
                dog[scale] = scales[scale] - scale[scales - 1];
            }

            int rowSize = dog[0].getRowSize();
            int columnSize = dog[0].getColumnSize();
            // fortran begin
            for (int k = 1; k <= scalesPerOctave; ++k) {

                points.clear();

                sobelGradX(gradX, dog[k]);
                sobelGradY(gradY, dog[k]);
                sobelGradX(grdXX, gradX);
                sobelGradY(gradXY, gradX);
                sobelGradY(gradYY, gradY);


                for (int i = border; i < rowSize - border; ++i)
                    for (int j = border; j < columnSize - border; ++j) {

                        bool isMaximum = true, isMinimum = true;
                        for (int t = 0; t < 26; ++t) {
                            if (dog[k].get(i, j) <= dog[k + dk26[t]].get(i + di26[t], j + dj26[t]))
                                isMaximum = false;
                            if (dog[k].get(i, j) >= dog[k + dk26[t]].get(i + di26[t], j + dj26[t]))
                                isMinimum = false;
                            if (!isMaximum && !isMinimum)
                                break;
                        }

                        if (!isMaximum && !isMinimum)
                            continue;
//                        qDebug() << "++";
                        auto dxx = grdXX.get(i, j), dxy = gradXY.get(i, j), dyy = gradYY.get(i, j),
                             tr  = dxx + dyy,
                             det = dxx * dyy - dxy * dxy;
                        if (tr * tr / det > 12.1)
                            continue;

                        auto dx = gradX.get(i, j), dy = gradY.get(i, j),
                             dj = -(  dx * dyy - dy * dxy) / det,
                             di = -(- dx * dxy + dy * dxx) / det;
//                        qDebug() << di << dj;
                        di = di < -1 ? -1 : di > 1 ? 1 : di;
                        dj = dj < -1 ? -1 : dj > 1 ? 1 : dj;
                        if (fabs(dog[k].get(i, j) - 0.5 * (dx * dj + dy * di)) < 0.02) {
//                            qDebug() << "--";
                            continue;
                        }

//                        qDebug() << j + dj << j << dj;

                        points.emplace_back(i + di, j + dj);
                    }

                double t = sp.getSigma(octave, k - 1) / sp.getSigma(octave, 0);
                int oldCnt = descriptors.size();
                getDescriptors(descriptors, gradX, gradY, points, t);
                double p = pow(2, octave);
                for_each(
                    begin(descriptors) + oldCnt,
                    end(descriptors),
                    [=](Descriptor &descriptor) {
                        descriptor.scale *= p;
                        descriptor.centerI *= p;
                        descriptor.centerJ *= p;

                    }
                );

            }

        }

    }

    ///*************************************************************************
    ///*****                 IMAGE MATCHING               **********************
    ///*************************************************************************

    void ransacAffineTransform(const Matrix &dest, const Matrix &src, double param[6], int countIteration, double maxDist, double eps)
    {
        auto srcRows = src.getRowSize();
        auto srcCols = src.getColumnSize();
        auto destRows = dest.getRowSize();
        auto destCols = dest.getColumnSize();

        vector<Descriptor> sd, dd;
        getDescriptors(sd, src);
        getDescriptors(dd, dest);

        qDebug() << "src  descriptors:" << sd.size();
        qDebug() << "dest descriptors:" << dd.size();

        auto srcImage = matrixToImage(src);
        auto destImage = matrixToImage(dest);
        QImage image(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);

        QPainter painter(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);
        vector<pair<int, int>> pairToDescriptor;

        vector<pair<Eigen::Vector3d, Eigen::Vector3d>> pairs;
        for (int i = 0; i < sd.size(); ++i) {
            for (int j = 0; j < dd.size(); ++j) {
                if (sd[i].distTo(dd[j]) <= maxDist) {
                    pairs.emplace_back();
                    pairs.back().first << (sd[i].centerI / (srcRows / 2) - 1), (sd[i].centerJ / (srcCols / 2) - 1), 1;
                    pairs.back().second << (dd[j].centerI / (destRows / 2) - 1), (dd[j].centerJ / (destCols / 2) - 1), 1;

                    pairToDescriptor.emplace_back(i, j);
                    painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
                    drawDesctiptor(painter, sd[i]);
                    drawDesctiptor(painter, dd[j], 0, srcImage->width());
                    painter.drawLine(
                            sd[i].centerJ, sd[i].centerI,
                            dd[j].centerJ + srcImage->width(), dd[j].centerI);
                }
            }
        }

        painter.end();
        image.save("test.png");

        qDebug() << "pairs:" << pairs.size();

        random_device rd;
        mt19937 mt(rd());
        uniform_int_distribution<int> rnd(0, pairs.size() - 1);

        Eigen::MatrixXd A(6, 6);
        Eigen::VectorXd B(6), x(6);
        Eigen::Matrix3d F;
        Eigen::Matrix3d M;

        int ansP[3];

        int bestAgree = 0;
        while (--countIteration >= 0) {

            if (countIteration % 1000 == 0)
                qDebug() << "rest:" << countIteration << "agree:" << bestAgree;

            int p[] = {rnd(mt), rnd(mt), rnd(mt)};

            auto &s0 = pairs[p[0]].first;
            auto &s1 = pairs[p[1]].first;
            auto &s2 = pairs[p[2]].first;
            auto &d0 = pairs[p[0]].second;
            auto &d1 = pairs[p[1]].second;
            auto &d2 = pairs[p[2]].second;

            A << s0(0), s0(1), 1,  0,     0,     0,
                 0,     0,     0,  s0(0), s0(1), 1,
                 s1(0), s1(1), 1,  0,     0,     0,
                 0,     0,     0,  s1(0), s1(1), 1,
                 s2(0), s2(1), 1,  0,     0,     0,
                 0,     0,     0,  s2(0), s2(1), 1;

            if (abs(A.determinant()) < eps)
                continue;

            B << d0(0),
                 d0(1),
                 d1(0),
                 d1(1),
                 d2(0),
                 d2(1);

            x = A.fullPivLu().solve(B);
            F << x(0), x(1), x(2),
                 x(3), x(4), x(5),
                 0,    0,    1;

            double relative_error = (A*x - B).norm() / B.norm();
            if (relative_error > eps)
                continue;
            auto t0 = d0 - F * s0,
                 t1 = d1 - F * s1,
                 t2 = d2 - F * s2;
            if (t0.norm() >= eps || t1.norm() >= eps || t2.norm() >= eps)
                continue;

            if (F.determinant() < 0.1)
                continue;

            int cntAgree = 0;
            for (auto &pair : pairs)
                if ((pair.second - F * pair.first).norm() < eps)
                    ++cntAgree;

            if (cntAgree > bestAgree) {
                bestAgree = cntAgree;
                M = F;

                for (int i = 0; i < 3; ++i)
                    ansP[i] = p[i];

            }
        }

        auto t0 = destRows / 2.0,
             t1 = destCols / 2.0,
             k0 = srcRows / 2.0,
             k1 = srcCols / 2.0;

        // Now we have T*x' = M*K*x
        // x' = (T^-1)*M*Kx
        // Transform matrix: (T^-1)*M*K
        Eigen::Matrix3d T, K;
        T << (1 / t0), 0,       -1,
              0,      (1 / t1), -1,
              0,       0,        1;
        K << (1 / k0), 0,       -1,
              0,      (1 / k1), -1,
              0,       0,        1;
        M = T.fullPivLu().inverse() * M * K;

        param[0] = M(0, 0);
        param[1] = M(0, 1);
        param[2] = M(0, 2);
        param[3] = M(1, 0);
        param[4] = M(1, 1);
        param[5] = M(1, 2);


        image = QImage(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);

        painter.begin(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);

        qDebug() << "Test:";

        for (int i = 0; i < 3; ++i) {
            int s = pairToDescriptor[ansP[i]].first, d = pairToDescriptor[ansP[i]].second;
            painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
            drawDesctiptor(painter, sd[s]);
            drawDesctiptor(painter, dd[d], 0, srcImage->width());
            painter.drawLine(
                    sd[s].centerJ, sd[s].centerI,
                    dd[d].centerJ + srcImage->width(), dd[d].centerI);

            auto fromI = sd[s].centerI,
                 fromJ = sd[s].centerJ,
                 toI = dd[d].centerI,
                 toJ = dd[d].centerJ;

            qDebug() << toI - (param[0] * fromI + param[1] * fromJ + param[2]);
            qDebug() << toJ - (param[3] * fromI + param[4] * fromJ + param[5]);

        }

        painter.end();
        image.save("best.png");

    }

    void ransacHomographyTransform(const Matrix &dest, const Matrix &src, double param[9], int countIteration, double maxDist, double eps)
    {
        auto srcRows = src.getRowSize();
        auto srcCols = src.getColumnSize();
        auto destRows = dest.getRowSize();
        auto destCols = dest.getColumnSize();

        vector<Descriptor> sd, dd;
        getDescriptors(sd, src);
        getDescriptors(dd, dest);

        qDebug() << "src  descriptors:" << sd.size();
        qDebug() << "dest descriptors:" << dd.size();

        auto srcImage = matrixToImage(src);
        auto destImage = matrixToImage(dest);
        QImage image(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);

        QPainter painter(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);
        vector<pair<int, int>> pairToDescriptor;

        vector<pair<Eigen::Vector3d, Eigen::Vector3d>> pairs;
        for (int i = 0; i < sd.size(); ++i) {
            for (int j = 0; j < dd.size(); ++j) {
                if (sd[i].distTo(dd[j]) <= maxDist) {
                    pairs.emplace_back();
                    pairs.back().first << (sd[i].centerI / (srcRows / 2) - 1), (sd[i].centerJ / (srcCols / 2) - 1), 1;
                    pairs.back().second << (dd[j].centerI / (destRows / 2) - 1), (dd[j].centerJ / (destCols / 2) - 1), 1;

                    pairToDescriptor.emplace_back(i, j);
                    painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
                    drawDesctiptor(painter, sd[i]);
                    drawDesctiptor(painter, dd[j], 0, srcImage->width());
                    painter.drawLine(
                            sd[i].centerJ, sd[i].centerI,
                            dd[j].centerJ + srcImage->width(), dd[j].centerI);
                }
            }
        }

        painter.end();
        image.save("test.png");

        qDebug() << "pairs:" << pairs.size();

        random_device rd;
        mt19937 mt(rd());
        uniform_int_distribution<int> rnd(0, pairs.size() - 1);

        Eigen::MatrixXd A(8, 8);
        Eigen::VectorXd B(8), x(8);
        Eigen::Matrix3d F, M;
        Eigen::VectorXd C(4);


        int ansP[4];

        int bestInliers = 0;
        while (--countIteration >= 0) {

            if (countIteration % 10000 == 0)
                qDebug() << "rest:" << countIteration << "agree:" << bestInliers;

            int p[] = {rnd(mt), rnd(mt), rnd(mt), rnd(mt)};

            auto &s0 = pairs[p[0]].first;
            auto &s1 = pairs[p[1]].first;
            auto &s2 = pairs[p[2]].first;
            auto &s3 = pairs[p[3]].first;
            auto &d0 = pairs[p[0]].second;
            auto &d1 = pairs[p[1]].second;
            auto &d2 = pairs[p[2]].second;
            auto &d3 = pairs[p[3]].second;

            A << s0(0), s0(1), 1,  0,     0,     0, (-d0(0) * s0(0)), (-d0(0) * s0(1)),//, -d0(0),
                 0,     0,     0,  s0(0), s0(1), 1, (-d0(1) * s0(0)), (-d0(1) * s0(1)),//, -d0(1),

                 s1(0), s1(1), 1,  0,     0,     0, (-d1(0) * s1(0)), (-d1(0) * s1(1)),//, -d1(0),
                 0,     0,     0,  s1(0), s1(1), 1, (-d1(1) * s1(0)), (-d1(1) * s1(1)),//, -d1(1),

                 s2(0), s2(1), 1,  0,     0,     0, (-d2(0) * s2(0)), (-d2(0) * s2(1)),//, -d2(0),
                 0,     0,     0,  s2(0), s2(1), 1, (-d2(1) * s2(0)), (-d2(1) * s2(1)),//, -d2(1),

                 s3(0), s3(1), 1,  0,     0,     0, (-d3(0) * s3(0)), (-d3(0) * s3(1)),//, -d3(0),
                 0,     0,     0,  s3(0), s3(1), 1, (-d3(1) * s3(0)), (-d3(1) * s3(1));//, -d3(1);

            Eigen::FullPivLU<Eigen::MatrixXd> lu(A);

            int rank = lu.rank();

            if (rank != 8)
                continue;

            B << d0(0),
                 d0(1),
                 d1(0),
                 d1(1),
                 d2(0),
                 d2(1),
                 d3(0),
                 d3(1);

            x = lu.solve(B);
//            x = A.fullPivLu().solve(B);
            F << x(0), x(1), x(2),
                 x(3), x(4), x(5),
                 x(6), x(7), 1;

            C << x(6) * s0(0) + x(7) * s0(1) + 1,
                 x(6) * s1(0) + x(7) * s1(1) + 1,
                 x(6) * s2(0) + x(7) * s2(1) + 1,
                 x(6) * s3(0) + x(7) * s3(1) + 1;

            double relative_error = (A*x - B).norm() / B.norm();
            if (relative_error > eps)
                continue;

            auto t0 = d0 * C(0) - F * s0,
                 t1 = d1 * C(1) - F * s1,
                 t2 = d2 * C(2) - F * s2,
                 t3 = d3 * C(3) - F * s3;
            if (t0.norm() >= eps || t1.norm() >= eps || t2.norm() >= eps || t3.norm() >= eps)
                continue;

            if (F.determinant() < 0.1) // dirty hack!!!
                continue;


            int cntInliers = 0;
            for (auto &pair : pairs) {
                auto &d = pair.second;
                auto fs = F * pair.first;
                if ((d * fs(2) - fs).norm() < eps)
                    ++cntInliers;
            }

            if (cntInliers > bestInliers) {
                bestInliers = cntInliers;
                M = F;

                for (int i = 0; i < 4; ++i)
                    ansP[i] = p[i];

            }
        }

        qDebug() << "";

        auto t0 = destRows / 2.0,
             t1 = destCols / 2.0,
             k0 = srcRows / 2.0,
             k1 = srcCols / 2.0;

        // Now we have T*x' = M*K*x
        // x' = (T^-1)*M*Kx
        // Transform matrix: (T^-1)*M*K
        Eigen::Matrix3d T, K;
        T << (1 / t0), 0,       -1,
              0,      (1 / t1), -1,
              0,       0,        1;
        K << (1 / k0), 0,       -1,
              0,      (1 / k1), -1,
              0,       0,        1;
        M = T.fullPivLu().inverse() * M * K;

        param[0] = M(0, 0);
        param[1] = M(0, 1);
        param[2] = M(0, 2);
        param[3] = M(1, 0);
        param[4] = M(1, 1);
        param[5] = M(1, 2);
        param[6] = M(2, 0);
        param[7] = M(2, 1);
        param[8] = M(2, 2);

        image = QImage(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);

        painter.begin(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);

        qDebug() << "After test:";

        for (int i = 0; i < 4; ++i) {
            int s = pairToDescriptor[ansP[i]].first, d = pairToDescriptor[ansP[i]].second;
            painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
            drawDesctiptor(painter, sd[s]);
            drawDesctiptor(painter, dd[d], 0, srcImage->width());
            painter.drawLine(
                    sd[s].centerJ, sd[s].centerI,
                    dd[d].centerJ + srcImage->width(), dd[d].centerI);

            auto fromI = sd[s].centerI,
                 fromJ = sd[s].centerJ,
                 toI = dd[d].centerI,
                 toJ = dd[d].centerJ;

            auto c = param[6] * fromI + param[7] * fromJ + param[8];
            qDebug() << toI * c - (param[0] * fromI + param[1] * fromJ + param[2]);
            qDebug() << toJ * c - (param[3] * fromI + param[4] * fromJ + param[5]);

        }

        painter.end();
        image.save("best.png");


    }

    void houghTransformSimple(const Matrix &dest, const Matrix &src, ParameterSpace &ps, double maxDist)
    {

        vector<Descriptor> sd, dd;
        getDescriptors(sd, src);
        getDescriptors(dd, dest);

        qDebug() << "src  descriptors:" << sd.size();
        qDebug() << "dest descriptors:" << dd.size();




        auto srcImage = matrixToImage(src);
        auto destImage = matrixToImage(dest);
        QImage image(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);

        QPainter painter(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);




        for (auto &s : sd)
            for (auto &d : dd) {
                if (s.distTo(d) > maxDist)
                    continue;

                auto y = s.centerI, x = s.centerJ;

                auto a = d.angle - s.angle;
                while (a < 0) a += 2 * M_PI;
                auto cosa = cos(a), sina = sin(a);
                auto temp = x * cosa - y * sina;
                y = x * sina + y * cosa;
                x = temp;

                auto m = d.scale / s.scale;
                x *= m; y *= m;

                auto dx = d.centerJ - x, dy = d.centerI - y;
//                qDebug() << "+  " << m << a << dx << dy;
                ps.add(m, a, dx, dy, 1);






                painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
                drawDesctiptor(painter, s);
                drawDesctiptor(painter, d, 0, srcImage->width());
                painter.drawLine(
                        s.centerJ, s.centerI,
                        d.centerJ + srcImage->width(), d.centerI);

            }

        painter.end();
        image.save("test.png");
    }

    void houghTransformSimple(const Matrix &dest, const Matrix &src, double param[6], double maxDist, double maxScale, double ds, double da, double dx, double dy)
    {
        double minX = -src.getColumnSize(), maxX = dest.getColumnSize();
        double minY = -src.getRowSize(), maxY = dest.getRowSize();
        ParameterSpace ps(maxScale, ds, da, minX, maxX, dx, minY, maxY, dy);

        houghTransformSimple(dest, src, ps, maxDist);
//        ps.normalize();
        double s, a, di, dj;
        ps.getMax(s, a, dj, di);

        qDebug() << "parameters: " << s << a << dj << di;

        auto cosa = cos(a), sina = sin(a);
        param[0] = s * cosa;
        param[1] = s * sina;
        param[2] = di;
        param[3] = -s * sina;
        param[4] = s * cosa;
        param[5] = dj;
    }

    void houghTransformNormalized(const Matrix &dest, const Matrix &src, double param[6], double maxDist, double maxScale, double ds, double da, double dx, double dy)
    {
        vector<Descriptor> sd, dd;
        getDescriptors(sd, src);
        getDescriptors(dd, dest);

        qDebug() << "src  descriptors:" << sd.size();
        qDebug() << "dest descriptors:" << dd.size();




        auto srcImage = matrixToImage(src);
        auto destImage = matrixToImage(dest);
        QImage image(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);
        QPainter painter(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);
        for (auto &s : sd)
            for (auto &d : dd) {
                if (s.distTo(d) > maxDist)
                    continue;
                painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
                drawDesctiptor(painter, s);
                drawDesctiptor(painter, d, 0, srcImage->width());
                painter.drawLine(
                        s.centerJ, s.centerI,
                        d.centerJ + srcImage->width(), d.centerI);
            }
        painter.end();
        image.save("test.png");



        auto srcRows = src.getRowSize(),
             srcCols = src.getColumnSize(),
             destRows = dest.getRowSize(),
             destCols = dest.getColumnSize(),
             size = max(max(srcRows, srcCols), max(destRows, destCols));
        auto t0 = size / 2.0,
             t1 = t0,// / 2.0,
             k0 = t1,//max(srcRows, srcCols) / 2.0,
             k1 = k0;//srcRows / 2.0;

        for (auto &d : dd)
            d.centerI = d.centerI / t0 - 1, d.centerJ = d.centerJ / t1 - 1;
        for (auto &s : sd)
            s.centerI = s.centerI / k0 - 1, s.centerJ = s.centerJ / k1 - 1;

        ParameterSpace ps(maxScale, ds, da, -2, 2, dx, -2, 2, dy);
        for (auto &s : sd)
            for (auto &d : dd) {
                if (s.distTo(d) > maxDist)
                    continue;

                auto y = s.centerI, x = s.centerJ;

                auto a = d.angle - s.angle;
                while (a < 0) a += 2 * M_PI;
                auto cosa = cos(a), sina = sin(a);
                auto temp = x * cosa - y * sina;
                y = x * sina + y * cosa;
                x = temp;

                auto m = d.scale / s.scale;
                x *= m; y *= m;

                auto dx = d.centerJ - x, dy = d.centerI - y;
                ps.add(m, a, dx, dy, 1);

            }


        double s, a, di, dj;
        ps.getMax(s, a, dj, di);
        qDebug() << "parameters: " << s << a << dj << di;
        auto cosa = cos(a), sina = sin(a);

        // Tx' = MKx
        Eigen::Matrix3d T, K, M;
        T << (1 / t0), 0,       -1,
              0,      (1 / t1), -1,
              0,       0,        1;
        K << (1 / k0), 0,       -1,
              0,      (1 / k1), -1,
              0,       0,        1;
        M <<  s * cosa, s * sina, di,
             -s * sina, s * cosa, dj,
             0,         0,        1;
        M = T.fullPivLu().inverse() * M * K;
        param[0] = M(0, 0);
        param[1] = M(0, 1);
        param[2] = M(0, 2);
        param[3] = M(1, 0);
        param[4] = M(1, 1);
        param[5] = M(1, 2);

        qDebug() << param[0] << param[1] << param[2];
        qDebug() << param[3] << param[4] << param[5];
    }

    struct Parameter {
        int s, a, x, y;

        Parameter(int s, int a, int x, int y) : s(s), a(a), x(x), y(y) {}

        bool operator<(const Parameter &p) const {
            if (s != p.s)
                return s < p.s;
            if (a != p.a)
                return a < p.a;
            if (x != p.x)
                return x < p.x;
            return y < p.y;
        }

    };

    void houghTransform(const Matrix &dest, const Matrix &src, double param[], double maxDist, int r)
    {
        auto ds = 0.125, da = M_PI / 8, _dx = 0.01, _dy = 0.01;

        vector<Descriptor> sd, dd;
        getDescriptors(sd, src);
        getDescriptors(dd, dest);

        qDebug() << "src  descriptors:" << sd.size();
        qDebug() << "dest descriptors:" << dd.size();


        auto srcImage = matrixToImage(src);
        auto destImage = matrixToImage(dest);
        QImage image(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);
        QPainter painter(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);
        for (auto &s : sd)
            for (auto &d : dd) {
                if (s.distTo(d) > maxDist)
                    continue;
                painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
                drawDesctiptor(painter, s);
                drawDesctiptor(painter, d, 0, srcImage->width());
                painter.drawLine(
                        s.centerJ, s.centerI,
                        d.centerJ + srcImage->width(), d.centerI);
            }
        painter.end();
        image.save("test.png");



        auto srcRows = src.getRowSize(),
             srcCols = src.getColumnSize(),
             destRows = dest.getRowSize(),
             destCols = dest.getColumnSize(),
             size = max(max(srcRows, srcCols), max(destRows, destCols));
        auto t0 = size / 2.0,
             t1 = t0,
             k0 = t1,
             k1 = k0;
        for (auto &d : dd)
            d.centerI = d.centerI / t0 - 1, d.centerJ = d.centerJ / t1 - 1;
        for (auto &s : sd)
            s.centerI = s.centerI / k0 - 1, s.centerJ = s.centerJ / k1 - 1;

        map<Parameter, vector<pair<int, int>>> ps;
        for (int i = 0; i < sd.size(); ++i)
            for (int j = 0; j < dd.size(); ++j) {
                Descriptor &s = sd[i], &d = dd[j];
                if (s.distTo(d) > maxDist)
                    continue;

                auto y = s.centerI, x = s.centerJ;

                auto a = d.angle - s.angle;
                while (a < 0) a += 2 * M_PI;
                auto cosa = cos(a), sina = sin(a);
                auto temp = x * cosa - y * sina;
                y = x * sina + y * cosa;
                x = temp;

                auto m = d.scale / s.scale;
                x *= m; y *= m;

                auto dx = d.centerJ - x, dy = d.centerI - y;

                int is = floor(m / ds) + 0.5;
                int ia = floor(a / da) + 0.5;
                int ix = floor(dx / _dx) + 0.5;
                int iy = floor(dy / _dy) + 0.5;



                for (int ds = -r; ds <= r; ++ds)
                    for (int da = -r; da <= r; ++da)
                        for (int dx = -r; dx <= r; ++dx)
                            for (int dy = -r; dy <= r; ++dy) {
                                Parameter p(is + ds, ia + da, ix + dx, iy + dy);

            //                    qDebug() << p.s << p.a << p.x << p.y;

                                if (!ps.count(p))
                                    ps[p] = vector<pair<int, int>>();
                                auto &psp = ps[p];
                                auto dp = make_pair(i, j);
                                if (find(begin(psp), end(psp), dp) == psp.end())
                                    psp.push_back(dp);
                            }

            }

        auto best = max_element(begin(ps), end(ps), [] (auto &p1, auto &p2) {return p1.second.size() < p2.second.size();});
        auto &pairs = best->second;
        qDebug() << "inliers: " << pairs.size();

        image = QImage(srcImage->width() + destImage->width(), max(srcImage->height(), destImage->height()), QImage::Format_RGB32);
        painter.begin(&image);
        painter.drawImage(0, 0, *srcImage);
        painter.drawImage(srcImage->width(), 0, *destImage);

        for (auto &pair : pairs) {
            qDebug() << pair.first << " " << pair.second;
            auto &s = sd[pair.first];
            auto &d = dd[pair.second];
//            qDebug() << s.centerI << s.centerJ << d.centerI << d.centerJ;
            painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
            drawDesctiptor(painter, s);
            drawDesctiptor(painter, d, 0, srcImage->width());
            painter.drawLine(
                    s.centerJ, s.centerI,
                    d.centerJ + srcImage->width(), d.centerI);
        }

        painter.end();
        image.save("best.png");

        if (pairs.size() >= 3) {

            int s0 = pairs[0].first;
            int d0 = pairs[0].second;
            int s1 = pairs[1].first;
            int d1 = pairs[1].second;
            int s2 = pairs.size() > 3 ? pairs[3].first : pairs[2].first;
            int d2 = pairs.size() > 3 ? pairs[3].second : pairs[2].second;

            Eigen::MatrixXd A(6, 6);
            Eigen::VectorXd B(6), x(6);
            Eigen::Matrix3d F;

            A << sd[s0].centerI, sd[s0].centerJ, 1,  0,              0,              0,
                 0,              0,              0,  sd[s0].centerI, sd[s0].centerJ, 1,
                 sd[s1].centerI, sd[s1].centerJ, 1,  0,              0,              0,
                 0,              0,              0,  sd[s1].centerI, sd[s1].centerJ, 1,
                 sd[s2].centerI, sd[s2].centerJ, 1,  0,              0,              0,
                 0,              0,              0,  sd[s2].centerI, sd[s2].centerJ, 1;

            B << dd[d0].centerI,
                 dd[d0].centerJ,
                 dd[d1].centerI,
                 dd[d1].centerJ,
                 dd[d2].centerI,
                 dd[d2].centerJ;

            x = A.fullPivLu().solve(B);

            F << x(0), x(1), x(2),
                 x(3), x(4), x(5),
                 0,    0,    1;

            // Tx' = MFx
            Eigen::Matrix3d T, K, M;
            T << (1 / t0), 0,       -1,
                  0,      (1 / t1), -1,
                  0,       0,        1;
            K << (1 / k0), 0,       -1,
                  0,      (1 / k1), -1,
                  0,       0,        1;

            M = T.fullPivLu().inverse() * F * K;
            param[0] = M(0, 0);
            param[1] = M(0, 1);
            param[2] = M(0, 2);
            param[3] = M(1, 0);
            param[4] = M(1, 1);
            param[5] = M(1, 2);

            qDebug() << "EEeeeeee";

        }
    }






}

