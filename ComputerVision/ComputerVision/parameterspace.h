#pragma once

#include <math.h>
#include <memory>
#include <map>
#include <QDebug>

using namespace std;

namespace cvl {

    class ParameterSpace
    {
    private:
        map<long, double> data;

        double maxScale;
        int cntScale;
        int cntAngle;
        double minX, maxX, minY, maxY;
        int cntX, cntY;

        double ds, da, dx, dy;
        double sum = 0;

        void addInside(int s, int a, int x, int y, double value);

    public:
//        ParameterSpace(double maxScale, int cntScale, int cntAngle, double minX, double maxX, int cntX, double minY, double maxY, int cntY);
        ParameterSpace(double maxScale, double ds, double da, double minX, double maxX, double dx, double minY, double maxY, double dy);
        ~ParameterSpace();

        void add(double s, double a, double x, double y, double value);
        void normalize();

        void getMax(double &s, double &a, double &x, double &y);
    };

}
