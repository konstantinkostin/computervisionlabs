#pragma once

#include <vector>
#include <math.h>
#include "matrix.h"
#include "descriptor.h"
#include "scalepyramid.h"
#include "parameterspace.h"

using namespace std;

namespace cvl {

    enum class TypeBorder {ZERO_BORDER, COPY_BORDER, REFLECT_BORDER, WRAP_BORDER};
    enum class TypeHarrisOperator {LAMBDA_MIN, ORIGINAL};

    void findLocalMaximum(vector<Point<int>> &findPoints, vector<double> &weights, const Matrix &matrix, int r, double t = 0, int border = 0);

    void convolution(Matrix &dest, const Matrix &src, const Matrix &kernel, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void convolutionSlow(Matrix &dest, const Matrix &src, const Matrix &kernel, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void gaussKernel(Matrix &kernel, int r);
    void gaussBlur(Matrix &dest, const Matrix &src, double sigma, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void gaussBlur(Matrix &dest, const Matrix &src, int r, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void gaussBlur(Matrix &dest, const Matrix &src, int r, double sigma, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void gaussBlur(Matrix &dest, const Matrix &src, double sigma, Matrix &buffer, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void gaussBlur(Matrix &dest, const Matrix &src, int r, Matrix &buffer, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void gaussBlur(Matrix &dest, const Matrix &src, int r, double sigma, Matrix &buffer, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void gaussBlurSlow(Matrix &dest, const Matrix &src, double sigma, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void downSample(Matrix &dest, const Matrix &src);

    void sobelGradX(Matrix &dest, const Matrix &src, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void sobelGradY(Matrix &dest, const Matrix &src, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void sobel(Matrix &dest, const Matrix &src, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void sobel(Matrix &dest, const Matrix &src, Matrix &gradX, Matrix &gradY, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void cannyEdgeDetecter(Matrix &dest, const Matrix &src, double low, double high, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void cannyEdgeDetecter(Matrix &dest, const Matrix &src, Matrix &gradX, Matrix &gradY, double low, double high, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void moravekOperator(Matrix &dest, const Matrix &src, int r, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void harrisOperator(Matrix &dest, const Matrix &src, int r, const TypeHarrisOperator typeOperator = TypeHarrisOperator::LAMBDA_MIN, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    pair<vector<Point<int>>, vector<double>> nonMaximumSuppression(const vector<Point<int>> &points, const vector<double> &weights, int count = 100, double r0 = 1, double scale = 1.5, double d = 0.9);

    void getAngles(vector<double> &angles, const Matrix &gradX, const Matrix &gradY, int posI, int posJ, int r, int o, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

//    Descriptor calcDescriptor(const Matrix &src, int posI, int posJ, int r, int o, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void getDescriptors(vector<Descriptor> &descriptors, const Matrix &gradX, const Matrix &gradY, vector<Point<double> > &points, double scale = 1, int r = 16, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);
    void getDescriptors(vector<Descriptor> &descriptors, const Matrix &src, int scalesPerOctave = 5, int r = 7, double s0 = 0.5, double s = 1.6, double t = 0.01, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void getDescriptors(vector<vector<double>> &descriptors, const Matrix &src, vector<Point<int>> &points, int r, int o, const TypeBorder typeBorder = TypeBorder::ZERO_BORDER);

    void ransacAffineTransform(const Matrix &dest, const Matrix &src, double param[6], int countIteration, double dist = 0.2, double eps = 0.01);
    void ransacHomographyTransform(const Matrix &dest, const Matrix &src, double param[9], int countIteration, double maxDist = 0.2, double eps = 0.01);

    void houghTransformSimple(const Matrix &dest, const Matrix &src, ParameterSpace &ps, double maxDist = 0.45);
    void houghTransformSimple(const Matrix &dest, const Matrix &src, double param[6], double maxDist = 0.45, double maxScale = 4, double ds = 0.125, double da = 0.0981, double dx = 1, double dy = 1); // 8 * 64 * 1024 * 1024 = 2^29
    void houghTransformNormalized(const Matrix &dest, const Matrix &src, double param[6], double maxDist = 0.45, double maxScale = 4, double ds = 0.125, double da = 0.0981, double dx = 1, double dy = 1);
    void houghTransform(const Matrix &dest, const Matrix &src, double param[6], double maxDist = 0.45, int r = 2);

}
