#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T00:28:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++14

TARGET = ComputerVision
TEMPLATE = app

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE += -O3


SOURCES += main.cpp\
        mainwindow.cpp \
    matrix.cpp \
    qutils.cpp \
    cvlutils.cpp \
    scalepyramid.cpp \
    descriptor.cpp \
    parameterspace.cpp

HEADERS  += mainwindow.h \
    matrix.h \
    qutils.h \
    cvlutils.h \
    scalepyramid.h \
    descriptor.h \
    parameterspace.h

FORMS    += mainwindow.ui
