#include "scalepyramid.h"

#include <QDebug>

namespace cvl {

    ScalePyramid::ScalePyramid()
    {

    }

    void ScalePyramid::build(const Matrix &initialMatrix, int octaves, int scalesPerOctave, double baseSigma, double sigma)
    {
        this->sigma = sigma;
        this->octaves = octaves;
        this->scalesPerOctave = scalesPerOctave;
        scalesSize = octaves * (scalesPerOctave + 3);
        sigmas = std::make_unique<double[]>(scalesSize);
        scales = std::make_unique<Matrix[]>(scalesSize);

        Matrix buffer;
        const double k = pow(2.0, 1.0 / scalesPerOctave);
        sigmas[1] = sigma;
        sigmas[0] = max(sigma / k, baseSigma);

        double ds = sqrt(sigmas[0] * sigmas[0] - baseSigma * baseSigma);
        gaussBlur(scales[0], initialMatrix, ds, buffer);
        sqrt(sigmas[1] * sigmas[1] - sigmas[0] * sigmas[0]);
        gaussBlur(scales[1], scales[0], ds, buffer);

        double s = sigma;
        for (int i = 2; i < scalesSize; ++i) {

            if (i % (scalesPerOctave + 3) <= 1) {

                downSample(scales[i], scales[i - 3]);
                s = sigma;
                sigmas[i] = sigmas[i - 3];

            } else {

                double newS = s * k;
                ds = sqrt(newS * newS - s * s);
                gaussBlur(scales[i], scales[i - 1], ds);
                s = newS;
                sigmas[i] = sigmas[i - 1] * k;

            }

        }

    }

    Matrix &ScalePyramid::getScale(int octave, int scale)
    {
        Q_ASSERT(octave >= 0 && octave < octaves && scale >= -1 && scale <= scalesPerOctave + 1);
        return scales[octave * (scalesPerOctave + 3) + scale + 1];
    }

    double ScalePyramid::getSigma(int octave, int scale)
    {
        Q_ASSERT(octave >= 0 && octave < octaves && scale >= -1 && scale <= scalesPerOctave + 1);
        return sigmas[octave * (scalesPerOctave + 3) + scale + 1];
    }

    double ScalePyramid::get(int i, int j, double t) const
    {
        t = std::min(std::max(t, sigma), sigmas[scalesSize - 1]);
        auto ptrBegin = &sigmas[0];
        auto ptrUpper = std::upper_bound(ptrBegin, ptrBegin + scalesSize, t);
        int index = ptrUpper - ptrBegin - 1; // index of last element less or equals t

        int log = index / (scalesPerOctave + 1);
        i >>= log;
        j >>= log;

        double ds = sigmas[index + 1] - sigmas[index],
               l = t - sigmas[index],
               r = sigmas[index + 1] - t;

        Q_ASSERT(ds > 1e-11);
        Q_ASSERT(l >= 0 && r >= 0);

        return (scales[index].get(i, j) * r + scales[index + 1].get(i, j) * l) / ds;
    }

    double ScalePyramid::get(int i, int j, int t) const
    {
        int log = t / (scalesPerOctave + 1);
        i >>= log;
        j >>= log;
        return scales[t].get(i, j);
    }

}

