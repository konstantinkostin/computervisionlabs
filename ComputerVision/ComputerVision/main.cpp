#include <iostream>

#include "mainwindow.h"
#include <QApplication>

#include <QImage>
#include <QDebug>
#include <QPainter>
#include <QTransform>
#include <QPicture>

#include "qutils.h"
#include "matrix.h"
#include "cvlutils.h"
#include "scalepyramid.h"
#include "descriptor.h"
#include "parameterspace.h"

using namespace cvl;

void lab1(int argc, char *argv[]) {
    QString fileName = (argc > 0) ? argv[0] : "image.jpg";
    auto matrix = imageToMatrix(QImage(fileName));

    matrixToImage(*matrix)->save("source.jpg");


    Matrix dx, dy, s;
    sobel(s, *matrix, dx, dy);
    dx.normalize();
    dy.normalize();
    s.normalize();

    matrixToImage(dx)->save("gradX.jpg");
    matrixToImage(dy)->save("gradY.jpg");
    matrixToImage(s)->save("sobel.jpg");

    Matrix blur, blurSlow;
    gaussBlur(blur, *matrix, 1); // it is separate
    gaussBlurSlow(blurSlow, *matrix, 1); // it is not

    qDebug() << "Separate kernel check:" << blur.equals(blurSlow, 1e-6);

    for (int i = 1; i < argc; ++i) {
        QString sigma(argv[i]);
        gaussBlur(blur, *matrix, sigma.toDouble());
        sobel(s, blur, dx, dy);
        s.normalize();
        matrixToImage(s)->save(QString("sobel") + QString::number(i) + ".jpg");
    }
}

void lab2(int argc, char *argv[]) {
    QString fileName = (argc > 0) ? argv[0] : "image.jpg";
    auto matrix = imageToMatrix(QImage(fileName));

    matrixToImage(*matrix)->save("source.jpg");

    int octaves;
    if (argc > 1)
        octaves = QString(argv[1]).toInt();
    else {
        int minDim = std::min(matrix->getRowSize(), matrix->getColumnSize());
        octaves = 0;
        while (minDim > 32) {
            octaves++;
            minDim >>= 1;
        }
    }
    int scales = (argc > 2) ? QString(argv[2]).toInt() : 4;
    double baseSigma = (argc > 3) ? QString(argv[3]).toDouble() : 0.5;
    double sigma = (argc > 4) ? QString(argv[4]).toDouble() : 1.6;

    ScalePyramid sp;
    sp.build(*matrix, octaves, scales, baseSigma, sigma);

    for (int i = 0; i < octaves; ++i)
        for (int j = -1; j <= scales + 1; ++j) {
            double s = sp.getSigma(i, j);
            QString str = QString::number(i + 1) + "_" + QString::number(j) + "(" + QString::number(s) + ").jpg";
            matrixToImage(sp.getScale(i, j))->save(str);
        }
}

void lab3(int argc, char *argv[]) {

    QString fileName = (argc > 1) ? argv[1] : "image.jpg";
    QImage image(fileName);
    auto matrix = imageToMatrix(image);
    matrixToImage(*matrix)->save("source.png");

    QString action((argc > 0) ? argv[0] : "canny");

    if (action == "canny") {
        Matrix edges, blur;
        gaussBlur(blur, *matrix, 0.5);
        cannyEdgeDetecter(edges, blur, 0.0001, 0.25);
        matrixToImage(edges)->save("canny.png");
    }

    if (action == "moravek") {

        int r = argc > 2 ? atoi(argv[2]) : 2;
        double t = argc > 3 ? atof(argv[3]) : 0.01;

        Matrix blur;
        gaussBlur(blur, *matrix, 2);

        Matrix moravek;
        moravekOperator(moravek, blur, r);
        moravek.normalize();
        matrixToImage(moravek)->save("moravek_raw.png");

        vector<Point<int>> points;
        vector<double> weights;
        findLocalMaximum(points, weights, moravek, 2, t);
        QImage moraverImage(image);
        addRedPoints(moraverImage, points, weights);
        moraverImage.save("moravek.png");

        auto rez = nonMaximumSuppression(points, weights, 150);
        auto newPoints = rez.first;
        auto newWeights = rez.second;
        QImage suppressionImage(image);
        addRedPoints(suppressionImage, newPoints, newWeights);
        suppressionImage.save("moravek_suppression.png");

        qDebug() << "allPoints:" << points.size();
        qDebug() << "supPoints:" << newPoints.size();

    }

    if (action == "harris") {

        int r = argc > 2 ? atoi(argv[2]) : 13;
        double t = argc > 3 ? atof(argv[3]) : 0.001;

        Matrix harris;
        harrisOperator(harris, *matrix, r, TypeHarrisOperator::ORIGINAL);
        harris.normalize();
        matrixToImage(harris)->save("harris_raw.png");

        vector<Point<int>> points;
        vector<double> weights;
        findLocalMaximum(points, weights, harris, 2, t);
        QImage harrisImage(image);
        addRedPoints(harrisImage, points, weights);
        harrisImage.save("harris.png");

        auto rez = nonMaximumSuppression(points, weights, 150);
        auto newPoints = rez.first;
        auto newWeights = rez.second;
        QImage suppressionImage(image);
        addRedPoints(suppressionImage, newPoints, newWeights);
        suppressionImage.save("harris_suppression.png");

        qDebug() << "allPoints:" << points.size();
        qDebug() << "supPoints:" << newPoints.size();

    }

}

double hungarian(vector<int> &ans, vector<vector<double>> &a) {
    int n = a.size() - 1;
    int m = a[0].size() - 1;
    vector<double> u(n + 1, 0), v(m + 1, 0);
    vector<int> p(m + 1), way(m + 1);
    for (int i = 1; i <= n; ++i) {
        p[0] = i;
        int j0 = 0;
        vector<double> minv(m + 1, 1e9);
        vector<bool> used(m + 1, false);
        do {
            used[j0] = true;
            int i0 = p[j0], j1;
            double delta = 1e9;
            for (int j = 1; j <= m; ++j)
                if (!used[j]) {
                    double cur = a[i0][j] - u[i0] - v[j];
                    if (cur < minv[j])
                        minv[j] = cur, way[j] = j0;
                    if (minv[j] < delta)
                        delta = minv[j], j1 = j;
                }
            for (int j = 0; j <= m; ++j)
                if (used[j])
                    u[p[j]] += delta, v[j] -= delta;
                else
                    minv[j] -= delta;
            j0 = j1;
        } while (p[j0] != 0);
        do {
            int j1 = way[j0];
            p[j0] = p[j1];
            j0 = j1;
        } while (j0);
    }

    ans.resize(n + 1);
    for (int j = 1; j <= m; ++j)
        ans[p[j]] = j;

    return -v[0];
}

double getDist(vector<double> &a, vector<double> &b) {
    int cnt = min(a.size(), b.size());
    double rez = 0;
    for (int i = 0; i < cnt; ++i)
        rez += (a[i] - b[i]) * (a[i] - b[i]);
    return sqrt(rez);
}

void lab4(int argc, char *argv[]) {
    QString fileName = (argc > 0) ? argv[0] : "image1.png";
    QImage image1(fileName);
    auto matrix1 = imageToMatrix(image1);
    fileName = (argc > 1) ? argv[1] : "image2.png";
    QImage image2(fileName);
    auto matrix2 = imageToMatrix(image2);
    int cnt1 = argc > 2 ? atoi(argv[2]) : 60;
    int cnt2 = argc > 3 ? atoi(argv[3]) : 150;

    int r = 8;
    double t = 0.03;
    int dSize = 16;
    int numOrientations = 8;

    Matrix harris;
    harrisOperator(harris, *matrix1, r);
    vector<Point<int>> points;
    vector<double> weights;
    findLocalMaximum(points, weights, harris, 2, t, dSize);
    auto rez1 = nonMaximumSuppression(points, weights, cnt1);
    vector<Point<int>> points1 = rez1.first;
    vector<double> weights1 = rez1.second;

    harrisOperator(harris, *matrix2, r);
    findLocalMaximum(points, weights, harris, 2, t, dSize);
    auto rez2 = nonMaximumSuppression(points, weights, cnt2);
    vector<Point<int>> points2 = rez2.first;
    vector<double> weights2 = rez2.second;

    vector<vector<double>> descriptors1, descriptors2;
    getDescriptors(descriptors1, *matrix1, points1, dSize, numOrientations);
    getDescriptors(descriptors2, *matrix2, points2, dSize, numOrientations);

    qDebug() << "point 1:" << descriptors1.size();
    qDebug() << "point 2:" << descriptors2.size();
    cnt1 = min(cnt1, (int) descriptors1.size());
    cnt2 = min(cnt2, (int) descriptors2.size());

    vector<vector<double>> a;
    a.push_back(vector<double>());
    a[0].resize(cnt2 + 1, 0);
    for (int i = 0; i < cnt1; ++i) {
        vector<double> distances;
        distances.push_back(0);
        for (int j = 0; j < cnt2; ++j) {
            distances.push_back(getDist(descriptors1[i], descriptors2[j]));
        }
        a.push_back(distances);
    }

    vector<int> ans;
//    qDebug() << "cost =" << hungarian(ans, a);
    ans.resize(cnt1 + 1);
    for (int i = 0; i < cnt1; ++i) {
        double minDist = getDist(descriptors1[i], descriptors2[0]);
        int v = 0;
        for (int j = 1; j < cnt2; ++j)
            if (minDist > getDist(descriptors1[i], descriptors2[j])) {
                minDist = getDist(descriptors1[i], descriptors2[j]);
                v = j;
            }
        ans[i + 1] = v + 1;
    }

    image1 = *matrixToImage(*matrix1);
    image2 = *matrixToImage(*matrix2);

    addRedPoints(image1, points1, weights1);
    addRedPoints(image2, points2, weights2);

    QImage image(image1.width() + image2.width(), max(image1.height(), image2.height()), QImage::Format_RGB32);
    QPainter painter(&image);
    painter.fillRect(image.rect(), QBrush(Qt::white));
    painter.drawImage(0, 0, image1);
    painter.drawImage(image1.width(), 0, image2);



    for (int i = 0; i < cnt1; ++i) {
        int j = ans[i + 1] - 1;
        painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
        painter.drawLine(points1[i].j, points1[i].i, points2[j].j + image1.width(), points2[j].i);
    }

    image.save("rez.png");
}

//void lab5(int argc, char *argv[]) {
//    QString fileName = (argc > 0) ? argv[0] : "image1.png";
//    QImage image1(fileName);
//    auto matrix1 = imageToMatrix(image1);
//    fileName = (argc > 1) ? argv[1] : "image2.png";
//    QImage image2(fileName);
//    auto matrix2 = imageToMatrix(image2);


//    int r = 8;
//    double t = 0.03;
//    int dSize = 16;
//    int numOrientations = 8;

//    Matrix harris;
//    harrisOperator(harris, *matrix1, r);
//    vector<Point<int>> points1;
//    vector<double> weights1;
//    findLocalMaximum(points1, weights1, harris, 2, t, dSize);

//    vector<Point<int>> points2;
//    vector<double> weights2;
//    harrisOperator(harris, *matrix2, r);
//    findLocalMaximum(points2, weights2, harris, 2, t, dSize);

//    vector<Descriptor> descriptors1, descriptors2;
//    getDescriptors(descriptors1, *matrix1, points1);
//    getDescriptors(descriptors2, *matrix2, points2);

//    qDebug() << "point 1:" << descriptors1.size();
//    qDebug() << "point 2:" << descriptors2.size();

//    double maxDist = 0.3;

//    int cnt = descriptors1.size();
//    vector<int> ans(cnt, -1);
//    for (int i = 0; i < cnt; ++i) {
//        double minDist = descriptors1[i].distTo(descriptors2[0]);
//        int v = 0;
//        for (int j = 1; j < descriptors2.size(); ++j)
//            if (minDist > descriptors1[i].distTo(descriptors2[j])) {
//                minDist = descriptors1[i].distTo(descriptors2[j]);
//                v = j;
//            }
//        if (minDist <= maxDist)
//            ans[i]= v;
//    }

//    addRedPoints(image1, points1, weights1);
//    addRedPoints(image2, points2, weights2);
//    QImage image(image1.width() + image2.width(), max(image1.height(), image2.height()), QImage::Format_RGB32);
//    QPainter painter(&image);
//    painter.drawImage(0, 0, image1);
//    painter.drawImage(image1.width(), 0, image2);



//    for (int i = 0; i < cnt; ++i) {
//        int j = ans[i];
//        if (j == -1)
//            continue;
//        painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
//        painter.drawLine(points1[i].j, points1[i].i, points2[j].j + image1.width(), points2[j].i);
////        qDebug() << sqrt((points1[i].j - points2[j].j) * (points1[i].j - points2[j].j) +
////                         (points1[i].i - points2[j].i) * (points1[i].i - points2[j].i));
//    }

//    image.save("rez.png");
//}

void lab6(int argc, char *argv[]) {
    QString fileName = (argc > 0) ? argv[0] : "image1.png";
    QImage image1(fileName);
    auto matrix1 = imageToMatrix(image1);
    fileName = (argc > 1) ? argv[1] : "image2.png";
    QImage image2(fileName);
    auto matrix2 = imageToMatrix(image2);


    int r = 8;
    double t = 0.03;
    int dSize = 16;
    int numOrientations = 8;

    Matrix harris;
    harrisOperator(harris, *matrix1, r);
    vector<Point<int>> points1;
    vector<double> weights1;
    findLocalMaximum(points1, weights1, harris, 2, t, dSize);

//    vector<Point> points2;
//    vector<double> weights2;
//    harrisOperator(harris, *matrix2, r);
//    findLocalMaximum(points2, weights2, harris, 2, t, dSize);

//    vector<Descriptor> descriptors1, descriptors2;
//    getDescriptors(descriptors1, *matrix1, points1, 16);
//    getDescriptors(descriptors2, *matrix2, points2, 16);

    vector<Descriptor> descriptors1, descriptors2;
    getDescriptors(descriptors1, *matrix1);
    getDescriptors(descriptors2, *matrix2);

    qDebug() << "point 1:" << descriptors1.size();
    qDebug() << "point 2:" << descriptors2.size();

    double maxDist = 0.3;
    int cnt = descriptors1.size();
    vector<int> ans(cnt, -1);
    for (int i = 0; i < cnt; ++i) {
        double minDist = descriptors1[i].distTo(descriptors2[0]);
        int v = 0;
        for (int j = 1; j < descriptors2.size(); ++j)
            if (minDist > descriptors1[i].distTo(descriptors2[j])) {
                minDist = descriptors1[i].distTo(descriptors2[j]);
                v = j;
            }
        qDebug() << "Mindist = " << minDist;
        if (minDist <= maxDist)
            ans[i]= v;
    }

//    for (Descriptor &descriptor : descriptors1)
//        drawDesctiptor(image1, descriptor);
//    for (Descriptor &descriptor : descriptors2)
//        drawDesctiptor(image2, descriptor);
//    image1.save("wata.png");
//    addRedPoints(image2, points2, weights2);

//    for (int i = 0; i < cnt; ++i)
//        if (ans[i] != -1) {
//            drawDesctiptor(image1, descriptors1[i]);
//            drawDesctiptor(image2, descriptors2[ans[i]]);
//        }
    QImage image(image1.width() + image2.width(), max(image1.height(), image2.height()), QImage::Format_RGB32);

    image1 = *matrixToImage(*matrix1);
    image2 = *matrixToImage(*matrix2);

    QPainter painter(&image);
    painter.fillRect(image.rect(), QBrush(Qt::white));
    painter.drawImage(0, 0, image1);
    painter.drawImage(image1.width(), 0, image2);



    for (int i = 0; i < cnt; ++i) {
        int j = ans[i];
        if (j == -1)
            continue;
        painter.setPen(QColor(abs(rand()) % 256, abs(rand()) % 256, abs(rand()) % 256));
        painter.drawLine(
                    descriptors1[i].centerJ, descriptors1[i].centerI,
                    descriptors2[j].centerJ + image1.width(), descriptors2[j].centerI);
        drawDesctiptor(painter, descriptors1[i]);
        drawDesctiptor(painter, descriptors2[j], 0, image1.width());
    }

    painter.end();
    image.save("rez.png");

//    addRedPoints(image1, descriptors1);
//    addRedPoints(image2, descriptors2);

    painter.begin(&image1);
    for (auto &d : descriptors1) {
        drawDesctiptor(painter, d);
    }
    painter.end();

    painter.begin(&image2);
    for (auto &d : descriptors2) {
        drawDesctiptor(painter, d);
    }
    painter.end();

    image1.save("points1.png");
    image2.save("points2.png");
}

void lab8(int argc, char *argv[]) {
    QString fileName = (argc > 0) ? argv[0] : "image1.png";
    QImage image1(fileName);
    auto matrix1 = imageToMatrix(image1);
    fileName = (argc > 1) ? argv[1] : "image2.png";
    QImage image2(fileName);
    auto matrix2 = imageToMatrix(image2);

    double h[9];
//    ransacAffineTransform(*matrix1, *matrix2, h, 1000000, 0.25, 0.01);
    ransacHomographyTransform(*matrix1, *matrix2, h, 1000000, 0.3, 0.01);

//    QTransform t(h[4], h[1], h[3], h[0], h[5], h[2]); // for affine transform
    QTransform t(h[4], h[1], h[7], h[3], h[0], h[6], h[5], h[2], h[8]); // for homography transformation
//    QTransform t(1, 2, 3, 4, 5, 6, 7, 8, 9);
    qDebug() << t.m11() << t.m12() << t.m13();
    qDebug() << t.m21() << t.m22() << t.m23();
    qDebug() << t.m31() << t.m32() << t.m33();

    qDebug() << t.isAffine();

    int width = 3000, height = 1500;
    QImage image(width, height, QImage::Format_RGB32);// = image1;//.transformed(t);
    QPainter painter(&image);
//    int dx = width / 2, dy = height / 2;
    int dx = 0, dy = 0;

    painter.drawImage(dx, dy, image1);

    painter.setTransform(t);
    painter.drawImage(dx, dy, image2);

    painter.end();

    image.save("transformed.png");


}

void lab9(int argc, char *argv[]) {
    QString fileName = (argc > 0) ? argv[0] : "image1.png";
    QImage image1(fileName);
    auto matrix1 = imageToMatrix(image1);
    fileName = (argc > 1) ? argv[1] : "image2.png";
    QImage image2(fileName);
    auto matrix2 = imageToMatrix(image2);

    double h[6];
//    houghTransformSimple(*matrix1, *matrix2, h, 0.3, 1, 0.05, M_PI / 16, 2, 2);
//    houghTransformSimple(*matrix1, *matrix2, h, 0.3, 1, 0.05, M_PI / 16, 2, 2);
//    houghTransformNormalized(*matrix1, *matrix2, h, 0.3, 2, 0.05, M_PI / 8, 0.001, 0.001);

//    houghTransform(*matrix1, *matrix2, h, 0.3, 0.15, M_PI / 8, 6, 6); // books
    houghTransform(*matrix1, *matrix2, h, 0.3, 3); // best
//    houghTransform(*matrix1, *matrix2, h, 0.45, 3);

//    return;

    QTransform t(h[4], h[1], h[3], h[0], h[5], h[2]);
    int width = image1.width(), height = image1.height();
    QImage image(width, height, QImage::Format_RGB32);// = image1;//.transformed(t);
    QPainter painter(&image);
    int dx = 0, dy = 0;

    painter.drawImage(dx, dy, image1);

    painter.setTransform(t);
    painter.setPen(QPen(QBrush(Qt::green), 10));
//    painter.setBrush(QBrush(Qt::green));
//    painter.drawImage(dx, dy, image2);
    painter.drawRect(0, 0, image2.width(), image2.height());

    painter.end();

    image.save("transformed.png");
}

int main(int argc, char *argv[])
{

    if (argc < 2)
        return 0;

    if (strcmp("lab1", argv[1]) == 0) {
        lab1(argc - 2, argv + 2);
    }

    if (strcmp("lab2", argv[1]) == 0) {
        lab2(argc - 2, argv + 2);
    }

    if (strcmp("lab3", argv[1]) == 0) {
        lab3(argc - 2, argv + 2);
    }

    if (strcmp("lab4", argv[1]) == 0) {
        lab4(argc - 2, argv + 2);
    }

//    if (strcmp("lab5", argv[1]) == 0) {
//        lab5(argc - 2, argv + 2);
//    }

    if (strcmp("lab6", argv[1]) == 0) {
        lab6(argc - 2, argv + 2);
    }

    if (strcmp("lab8", argv[1]) == 0) {
        lab8(argc - 2, argv + 2);
    }

    if (strcmp("lab9", argv[1]) == 0) {
        lab9(argc - 2, argv + 2);
    }

    return 0;
}
