#pragma once

#include <memory>
#include <QImage>
#include <QPainter>
#include <vector>
#include "matrix.h"
#include "descriptor.h"

using namespace std;

namespace cvl {

    enum class GrayScale {PAL, HDTV};

    std::shared_ptr<Matrix> imageToMatrix(const QImage &image, GrayScale grayScale = GrayScale::PAL);
    std::shared_ptr<QImage> matrixToImage(const Matrix &matrix);
    void addRedPoints(QImage &image, const Matrix &matrix, double e = 0.5);
    void addRedPoints(QImage &image, const vector<Point<int>> &points, const vector<double> &weights);
    void addRedPoints(QImage &image, const vector<Point<int>> &points);
    void addRedPoints(QImage &image, const vector<Descriptor> &descriptors);
    void drawDesctiptor(QImage &image, Descriptor &descriptor);
    void drawDesctiptor(QPainter &painter, Descriptor &descriptor, int di = 0, int dj = 0);

}
