#pragma once

#include <math.h>
#include <memory>
#include <QDebug>

using namespace std;

namespace cvl {

    class Descriptor
    {
    private:
        static const int size = 4;
        static const int numOrientation = 8;
        static const int descriptorSize = size * size * numOrientation;
//        std::unique_ptr<double[]> data = make_unique<double[]>(size * size * numOrientation);
        double data[descriptorSize];

        inline void add(int i, int j, int angle, double value) {
            if (i < 0 || i >= size || j < 0 || j >= size)
                return;
            angle = (angle % numOrientation + numOrientation) % numOrientation;
            data[i * size * numOrientation + j * numOrientation + angle] += value;
        }

    public:
        double centerI, centerJ;
        double angle;
        double scale = 1;

        Descriptor(double centerI, double centerJ, double angle);
        Descriptor(Descriptor &&descriptor) = default;
        ~Descriptor();

        void add(double i, double j, double angle, double value);
        double distTo(const Descriptor &descriptor);
        void normalize();

        inline double getAngle() {
            return angle;
        }
        inline void setAngle(double angle) {
            this->angle = angle;
        }
        inline double getScale() {
            return scale;
        }
        inline void setScale(double scale) {
            this->scale = scale;
        }
    };
}

