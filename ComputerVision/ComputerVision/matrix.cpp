#include "matrix.h"

#include <QDebug>

namespace cvl {

    Matrix::Matrix()
    {

    }

    Matrix::Matrix(int rowSize, int columnSize) : rowSize(rowSize),
                                                  columnSize(columnSize),
                                                  bufferSize(rowSize * columnSize)
    {
        buffer = std::make_unique<double[]>(bufferSize);
    }

    Matrix::Matrix(int rowSize, int columnSize, const double *data) : rowSize(rowSize),
                                                                      columnSize(columnSize),
                                                                      bufferSize(rowSize * columnSize)
    {
        buffer = std::make_unique<double[]>(bufferSize);
        for (int i = 0; i < bufferSize; ++i) {
            buffer[i] = data[i];
        }
    }

    Matrix::Matrix(int rowSize, int columnSize, std::unique_ptr<double[]> data) : rowSize(rowSize),
                                                                                  columnSize(columnSize),
                                                                                  bufferSize(rowSize * columnSize),
                                                                                  buffer(std::move(data))
    {
    }

    Matrix::Matrix(const Matrix &matrix) : rowSize(matrix.rowSize),
                                           columnSize(matrix.columnSize),
                                           bufferSize(rowSize * columnSize)
    {
        buffer = std::make_unique<double[]>(bufferSize);
        for (int i = 0; i < bufferSize; ++i)
            buffer[i] = matrix.buffer[i];
    }

    double &Matrix::at(int i, int j)
    {
        Q_ASSERT(i >= 0 && i < rowSize && j >= 0 && j < columnSize);
        return buffer[i * columnSize + j];
    }

    void Matrix::reduce(const Matrix &a, const Matrix &b, std::function<double (double, double)> f)
    {
        const int rowSize = a.getRowSize();
        const int columnSize = a.getColumnSize();
        if (rowSize != b.getRowSize() || columnSize != b.getColumnSize()) {
            resize(0, 0);
            return;
        }

        resize(rowSize, columnSize);
        const double *aBuffer = a.getBuffer();
        const double *bBuffer = b.getBuffer();
        enumerate1d([=](int i, double &x) {x = f(aBuffer[i], bBuffer[i]); });
    }

    void Matrix::resize(int rowSize, int columnSize)
    {
        if (rowSize * columnSize > bufferSize) {
            bufferSize = rowSize * columnSize;
            buffer = std::make_unique<double[]>(bufferSize);
        }
        this->rowSize = rowSize;
        this->columnSize = columnSize;
    }

    void Matrix::normalize()
    {
        double maxValue = *std::max_element(&buffer[0], &buffer[0] + rowSize * columnSize); // error
        for_each([=](auto &value) { value /= maxValue; });
    }

    void Matrix::normalize(double t0, double t1, double x0, double x1)
    {
        double d = t0 - t1;
        double a = (x0 - x1) / d;
        double b = (t0 * x1 - t1 * x0) / d;
        for_each([=](auto &value) { value = value * a + b; });
    }

    void Matrix::normalize(double x0, double x1)
    {
        auto values = std::minmax(&buffer[0], &buffer[0] + rowSize * columnSize);
        normalize(*values.first, *values.second, x0, x1);
    }

    void Matrix::scalarDivide(double d)
    {
        for_each([=](auto &value) { value /= d; });
    }

    void Matrix::hypot(const Matrix &a, const Matrix &b)
    {
        reduce(a, b, [](auto x, auto y) { return sqrt(x * x + y * y); });
    }

    Matrix Matrix::hypot(const Matrix &matrix) const
    {
        Matrix rez(rowSize, columnSize);
        rez.hypot(*this, matrix);
        return rez;
    }

    void Matrix::sqrSumm(const Matrix &a, const Matrix &b)
    {
        reduce(a, b, [](auto x, auto y) { return x * x + y * y; });
    }

    Matrix Matrix::sqrSumm(const Matrix &matrix) const
    {
        Matrix rez(rowSize, columnSize);
        rez.sqrSumm(*this, matrix);
        return rez;
    }

    void Matrix::scalarMultiply(const Matrix &a, const Matrix &b)
    {
        reduce(a, b, [](auto x, auto y) { return x * y; });
    }

    Matrix Matrix::scalarMultiply(const Matrix &matrix) const
    {
        Matrix rez(rowSize, columnSize);
        rez.scalarMultiply(*this, matrix);
        return rez;
    }

    const Matrix &Matrix::operator=(const Matrix &matrix)
    {
        rowSize = matrix.rowSize;
        columnSize = matrix.columnSize;
        bufferSize = rowSize * columnSize;
        buffer = std::make_unique<double[]>(bufferSize);
        for (int i = 0; i < bufferSize; ++i)
            buffer[i] = matrix.buffer[i];
        return *this;
    }

    Matrix Matrix::operator-(const Matrix &matrix) const
    {
        Matrix rez;
        rez.reduce(*this, matrix, [] (auto x, auto y) { return x - y; });
        return rez;
    }

    Matrix Matrix::operator*(const Matrix &matrix) const
    {
        const int n = rowSize;
        const int t = columnSize;
        const int m = matrix.columnSize;
        if (t != matrix.getRowSize())
            return Matrix();

        Matrix rez(n, m);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j) {
                double value = 0;
                for (int k = 0; k < t; ++k)
                    value += get(i, k) * matrix.get(k, j);
                rez.set(i, j, value);
            }

        return rez;
    }

    bool Matrix::equals(const Matrix &matrix, double eps) const
    {
        if (rowSize != matrix.rowSize)
            return false;
        if (columnSize != matrix.columnSize)
            return false;
        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j)
                if (fabs(get(i, j) - matrix.get(i, j)) > eps)
                    return false;

        return true;
    }



}

