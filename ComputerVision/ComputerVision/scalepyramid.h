#pragma once

#include "cvlutils.h"

namespace cvl {

    class ScalePyramid
    {
    private:
        int octaves,
            scalesPerOctave,
            scalesSize;
        double sigma;

        std::unique_ptr<double[]> sigmas;
        std::unique_ptr<Matrix[]> scales;

    public:
        ScalePyramid();

        void build(const Matrix &initialMatrix, int octaves, int scalesPerOctave, double baseSigma = 0.5, double sigma = 1.06);

        Matrix &getScale(int octave, int scale);
        double getSigma(int octave, int scale);
        double get(int i, int j, double t) const;
        double get(int i, int j, int t) const;
    };

}
