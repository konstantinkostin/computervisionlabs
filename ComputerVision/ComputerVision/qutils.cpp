#include "qutils.h"

namespace cvl {

    std::shared_ptr<Matrix> imageToMatrix(const QImage &image, GrayScale grayScale)
    {
        const int rowSize = image.height();
        const int columnSize = image.width();
        auto matrix = std::make_shared<Matrix>(rowSize, columnSize);

        for (int i = 0; i < rowSize; ++i)
            for (int j = 0; j < columnSize; ++j) {
                QRgb color = image.pixel(j, i);
                int r = qRed(color),
                    g = qGreen(color),
                    b = qBlue(color);

                double value;
                switch (grayScale) {
                case GrayScale::PAL:
                     value = (0.299 * r + 0.587 * g + 0.114 * b) / 255.0;
                     break;
                case GrayScale::HDTV:
                     value = (0.2126 * r + 0.7152 * g + 0.0722 * b) / 255.0;
                     break;
                }

                matrix->set(i, j, value);
            }

        return matrix;
    }

    inline int toColor(double value) {
        int color = fabs(value) * 255;
        color = std::min(255, color);
        return color;
    }

    std::shared_ptr<QImage> matrixToImage(const Matrix &matrix)
    {
        const int width = matrix.getColumnSize();
        const int height = matrix.getRowSize();
        auto image = std::make_shared<QImage>(width, height, QImage::Format_RGB32);
        for (int i = 0; i < height; ++i)
            for (int j = 0; j < width; ++j) {
                double value = matrix.get(i, j);
                int color = toColor(value);
                image->setPixel(j, i, (color << 16) | (color << 8) | color);
            }

        return image;
    }

    void addRedPoints(QImage &image, const vector<Point<int> > &points, const vector<double> &weights)
    {
        const int di[] = {-1, -1, -1,  0, 0,  1, 1, 1,  0, 2, 0, -2};
        const int dj[] = {-1,  0,  1, -1, 1, -1, 0, 1, -2, 0, 2,  0};
        const int cnt = 8;

        auto maxValue = *max_element(begin(weights), end(weights));

        for (size_t index = 0; index < points.size(); ++index) {
            int i = points[index].i;
            int j = points[index].j;
            int color = (weights[index] / maxValue) * 255;
            color = min(max(0, color), 255);
            image.setPixel(j, i, (color << 16) | (color << 8) | color);
            for (int k = 0; k < cnt; ++k) {
                const int toI = i + di[k];
                const int toJ = j + dj[k];
                if (toI >= 0 && toI < image.height() && toJ >= 0 && toJ < image.width())
                    image.setPixel(toJ, toI, 255 << 16);
            }
        }
    }

    void addRedPoints(QImage &image, const vector<Point<int>> &points)
    {
        vector<double> weights(points.size(), 1);
        addRedPoints(image, points, weights);
    }

    void drawDesctiptor(QImage &image, Descriptor &descriptor)
    {
        QPainter painter(&image);
        drawDesctiptor(painter, descriptor);
    }

    void drawDesctiptor(QPainter &painter, Descriptor &descriptor, int di, int dj)
    {
        int r = descriptor.scale * 16;
        painter.drawLine(descriptor.centerJ + dj, descriptor.centerI + di,
                         descriptor.centerJ + dj + r * cos(descriptor.angle), descriptor.centerI + di + r * sin(descriptor.angle));
        painter.drawEllipse(descriptor.centerJ + dj - r, descriptor.centerI + di - r, 2 * r, 2 * r);
    }

    void addRedPoints(QImage &image, const vector<Descriptor> &descriptors)
    {
        vector<Point<int>> points;
        for (const Descriptor &descriptor : descriptors) {
            points.emplace_back(descriptor.centerI + 0.5, descriptor.centerJ + 0.5);
        }
        addRedPoints(image, points);
    }




}

