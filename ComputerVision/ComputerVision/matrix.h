#pragma once

#include <QImage>
#include <memory>
#include <algorithm>
#include <QDebug>

#include "exception"
#include <functional>

namespace cvl {

    template <typename T = int>
    struct Point {
        T i, j;
        Point (T i, T j) : i(i), j(j) {}
//        int i, j;
//        Point (int i, int j) : i(i), j(j) {}
    };

    class Matrix
    {
    private:
        int rowSize = 0,
            columnSize = 0,
            bufferSize = 0;

        std::unique_ptr<double[]> buffer = nullptr;

    public:
        Matrix();
        Matrix(int rowSize, int columnSize);
        Matrix(int rowSize, int columnSize, const double *data);
        Matrix(int rowSize, int columnSize, std::unique_ptr<double[]> data);
        Matrix(const Matrix &matrix);
        Matrix(Matrix &&matrix) = default;

        inline int getRowSize() const { return rowSize; }
        inline int getColumnSize() const { return columnSize; }

        double &at(int i, int j);
        inline double get(int i, int j) const {
            Q_ASSERT(i >= 0 && i < rowSize && j >= 0 && j < columnSize);
            return buffer[i * columnSize + j];
        }
        inline void set(int i, int j, double value) {
            Q_ASSERT(i >= 0 && i < rowSize && j >= 0 && j < columnSize);
            buffer[i * columnSize + j] = value;
        }
        inline double *getBuffer() { return buffer.get(); }
        inline const double*getBuffer() const { return buffer.get(); }

        template<typename Functor>
        void for_each(Functor f) {
            std::for_each(&buffer[0], &buffer[0] + rowSize * columnSize, f);
        }

        template<typename Functor>
        void enumerate1d(Functor f) {
            const int count = rowSize * columnSize;
            for (int i = 0; i < count; ++i)
                f(i, buffer[i]);
        }

        template<typename Functor>
        void enumerate2d(Functor f) {
            for (int i = 0, index = 0; i < rowSize; ++i)
                for (int j = 0; j < columnSize; ++j, ++index)
                    f(i, j, buffer[index]);
        }

        /*!
         * Each element of current matrix will be calculate
         * on basis of function f.
         * thisMatrix[i][j] = f(a[i][j], b[i][j])
         */
        void reduce(const Matrix &a, const Matrix &b, std::function<double (double, double)> f); // TODO how to function to template???

        /**
         * Hard resize matrix without save any information
         */
        void resize(int rowSize, int columnSize);
        void normalize();
        void normalize(double t0, double t1, double x0, double x1);
        void normalize(double x0, double x1);
        void scalarDivide(double d);

        void hypot(const Matrix &a, const Matrix &b);
        Matrix hypot(const Matrix &matrix) const;
        void sqrSumm(const Matrix &a, const Matrix &b);
        Matrix sqrSumm(const Matrix &matrix) const;
        void scalarMultiply(const Matrix &a, const Matrix &b);
        Matrix scalarMultiply(const Matrix &matrix) const;

        const Matrix &operator=(const Matrix &matrix);
        Matrix operator-(const Matrix &matrix) const;
        Matrix operator*(const Matrix &matrix) const;

        bool equals(const Matrix &matrix, double eps = 1e-6) const;


    };



}


