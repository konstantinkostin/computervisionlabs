#include "descriptor.h"

namespace cvl {

    Descriptor::Descriptor(double centerI, double centerJ, double angle) : centerI(centerI), centerJ(centerJ), angle(angle) {
        memset(data, 0, sizeof(double) * descriptorSize);
    }

    Descriptor::~Descriptor() { }

    void Descriptor::add(double i, double j, double angle, double value)
    {
        const double sigma = 2;
        value = value * exp(-(i * i + j * j) / (2 * sigma * sigma));// / (2 * M_PI * sigma * sigma);
        i = i * (size / 2) + size / 2 - 0.5;
        j = j * (size / 2) + size / 2 - 0.5;
        angle = angle * numOrientation / (2 * M_PI);

        int x = floor(j) + 0.5;
        double vx[] = {x + 1 - j, 1 - vx[0]};
        int y = floor(i) + 0.5;
        double vy[] = {y + 1 - i, 1 - vy[0]};
        int a = floor(angle) + 0.5;
        double va[] = {a + 1 - angle, 1 - va[0]};
        for (int dx = 0; dx <= 1; ++dx)
            for (int dy = 0; dy <= 1; ++dy)
                for (int da = 0; da <= 1; ++da)
                    add(x + dx, y + dy, a + da, value * (vx[dx] + vy[dy] + va[da]));
    }

    double Descriptor::distTo(const Descriptor &descriptor)
    {
        if (size != descriptor.size || numOrientation != descriptor.numOrientation)
            return 1e9;

        double d = 0;
        for (int i = 0; i < descriptorSize; ++i) {
            d += (data[i] - descriptor.data[i]) * (data[i] - descriptor.data[i]);
        }
        return sqrt(d);
    }

    void Descriptor::normalize()
    {
        double len = 0;
        for_each(data, data + descriptorSize, [&] (double x) {len += x * x;});
        len = sqrt(len);
        bool again = false;
        for_each(data, data + descriptorSize, [len, &again] (double &x) {x /= len; if (x > 0.2) x = 0.2, again = true;});

        if (again) {
            len = 0;
            for_each(data, data + descriptorSize, [&] (double x) {len += x * x;});
            len = sqrt(len);
            for_each(data, data + descriptorSize, [=] (double &x) {x /= len;});
        }
    }

}

